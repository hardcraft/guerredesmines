package com.gmail.val59000mc.guerredesmines.spawners;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.guerredesmines.items.CustomItems;
import com.gmail.val59000mc.villagerapi.VillagerTrade;
import com.gmail.val59000mc.villagerapi.VillagerTradeApi;

public class CreeperVillager {

	Location location;
	
	public CreeperVillager(Location location) {
		this.location = location;
	}

	public void spawn() {
		World world = location.getWorld();
		Collection<Entity> entities =world.getNearbyEntities(location, 1, 1, 1);
		if(entities.size() > 0){
			Entity entity = entities.iterator().next();
			if(entity instanceof Villager){
				Villager villager = (Villager) entity;
				VillagerTradeApi.clearTrades(villager);
				
				VillagerTrade tradeA = new VillagerTrade(new ItemStack(Material.SULPHUR, 10), CustomItems.getCoalItem());
				VillagerTrade tradeB = new VillagerTrade(new ItemStack(Material.SULPHUR, 10), CustomItems.getBlindnessPotion());
				VillagerTrade tradeC = new VillagerTrade(new ItemStack(Material.SULPHUR, 15), CustomItems.getWitherPotion());
				VillagerTrade tradeD = new VillagerTrade(new ItemStack(Material.SULPHUR, 45), CustomItems.getC4());
				
				VillagerTradeApi.addTrade(villager, tradeA);
				VillagerTradeApi.addTrade(villager, tradeB);
				VillagerTradeApi.addTrade(villager, tradeC);
				VillagerTradeApi.addTrade(villager, tradeD);
			}
		}
	}

}
