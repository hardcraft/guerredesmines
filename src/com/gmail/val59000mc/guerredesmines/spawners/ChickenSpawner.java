package com.gmail.val59000mc.guerredesmines.spawners;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

public class ChickenSpawner extends Spawner {

	public ChickenSpawner(Location location, int limit, int time) {
		super(location, EntityType.CHICKEN, limit, time);
	}

}
