package com.gmail.val59000mc.guerredesmines.spawners;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Skeleton;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;

public class SkeletonSpawner extends Spawner {

	private List<ItemStack> helmets = Lists.newArrayList();
	private List<ItemStack> chestplates = Lists.newArrayList();
	private List<ItemStack> leggings = Lists.newArrayList();
	private List<ItemStack> boots = Lists.newArrayList();
	
	private List<Enchantment> helmetsEnchantments = Lists.newArrayList();
	private List<Enchantment> chestplatesEnchantments = Lists.newArrayList();
	private List<Enchantment> leggingsEnchantments = Lists.newArrayList();
	private List<Enchantment> bootsEnchantments = Lists.newArrayList();
	
	public SkeletonSpawner(Location location, int limit, int time) {
		super(location, EntityType.SKELETON, limit, time);
		
		// helmets
		this.helmets.add(new ItemStack(Material.LEATHER_HELMET));
		this.helmets.add(new ItemStack(Material.CHAINMAIL_HELMET));
		this.helmets.add(new ItemStack(Material.GOLD_HELMET));
		
		
		this.helmetsEnchantments.add(Enchantment.DURABILITY);
		this.helmetsEnchantments.add(Enchantment.OXYGEN);
		this.helmetsEnchantments.add(Enchantment.PROTECTION_ENVIRONMENTAL);
		this.helmetsEnchantments.add(Enchantment.PROTECTION_EXPLOSIONS);
		this.helmetsEnchantments.add(Enchantment.PROTECTION_FIRE);
		this.helmetsEnchantments.add(Enchantment.PROTECTION_PROJECTILE);
		this.helmetsEnchantments.add(Enchantment.THORNS);
		this.helmetsEnchantments.add(Enchantment.WATER_WORKER);
		
		
		// chestplate
		this.chestplates.add(new ItemStack(Material.LEATHER_CHESTPLATE));
		this.chestplates.add(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
		this.chestplates.add(new ItemStack(Material.GOLD_CHESTPLATE));
		
		this.chestplatesEnchantments.add(Enchantment.DURABILITY);
		this.chestplatesEnchantments.add(Enchantment.PROTECTION_ENVIRONMENTAL);
		this.chestplatesEnchantments.add(Enchantment.PROTECTION_EXPLOSIONS);
		this.chestplatesEnchantments.add(Enchantment.PROTECTION_FIRE);
		this.chestplatesEnchantments.add(Enchantment.PROTECTION_PROJECTILE);
		this.chestplatesEnchantments.add(Enchantment.THORNS);
		
		// leggings
		this.leggings.add(new ItemStack(Material.LEATHER_LEGGINGS));
		this.leggings.add(new ItemStack(Material.CHAINMAIL_LEGGINGS));
		this.leggings.add(new ItemStack(Material.GOLD_LEGGINGS));

		this.leggingsEnchantments.add(Enchantment.DURABILITY);
		this.leggingsEnchantments.add(Enchantment.PROTECTION_ENVIRONMENTAL);
		this.leggingsEnchantments.add(Enchantment.PROTECTION_EXPLOSIONS);
		this.leggingsEnchantments.add(Enchantment.PROTECTION_FIRE);
		this.leggingsEnchantments.add(Enchantment.PROTECTION_PROJECTILE);
		this.leggingsEnchantments.add(Enchantment.THORNS);
		
		// boots
		this.boots.add(new ItemStack(Material.LEATHER_BOOTS));
		this.boots.add(new ItemStack(Material.CHAINMAIL_BOOTS));
		this.boots.add(new ItemStack(Material.GOLD_BOOTS));

		this.bootsEnchantments.add(Enchantment.DEPTH_STRIDER);
		this.bootsEnchantments.add(Enchantment.DURABILITY);
		this.bootsEnchantments.add(Enchantment.PROTECTION_ENVIRONMENTAL);
		this.bootsEnchantments.add(Enchantment.PROTECTION_EXPLOSIONS);
		this.bootsEnchantments.add(Enchantment.PROTECTION_FALL);
		this.bootsEnchantments.add(Enchantment.PROTECTION_FIRE);
		this.bootsEnchantments.add(Enchantment.PROTECTION_PROJECTILE);
		this.bootsEnchantments.add(Enchantment.THORNS);
	}

	@Override
	public Entity spawn() {
		
		Entity entity = super.spawn();
		if(entity != null){
			customizeMob(((Skeleton) entity));
		}
		
		return entity;
	}
	


	private void customizeMob(Skeleton skeleton) {
		
		EntityEquipment equipment = skeleton.getEquipment();

		equipment.setHelmet(getRandomItems(helmets, helmetsEnchantments));
		equipment.setHelmetDropChance(0.15f);

		equipment.setChestplate(getRandomItems(chestplates, chestplatesEnchantments));
		equipment.setChestplateDropChance(0.15f);

		equipment.setLeggings(getRandomItems(leggings, leggingsEnchantments));
		equipment.setLeggingsDropChance(0.15f);

		equipment.setBoots(getRandomItems(boots, bootsEnchantments));
		equipment.setBootsDropChance(0.15f);
		
		skeleton.setHealth(1);
		
	}

	private ItemStack getRandomItems(List<ItemStack> items, List<Enchantment> enchantments){
		ItemStack item = items.get(Randoms.randomInteger(0, items.size() - 1)).clone();
		item.addUnsafeEnchantment(enchantments.get(Randoms.randomInteger(0, enchantments.size() - 1)), Randoms.randomInteger(1, 2));
		return item;
	}
}
