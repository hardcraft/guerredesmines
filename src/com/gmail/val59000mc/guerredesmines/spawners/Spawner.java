package com.gmail.val59000mc.guerredesmines.spawners;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public abstract class Spawner {

	protected EntityType type;
	protected Location location;
	protected int limit;
	private int time;
	
	
	
	public Spawner(Location location, EntityType type, int limit, int time) {
		super();
		this.location = location;
		this.type = type;
		this.limit = limit;
		this.time = time;
	}

	

	public int getTime() {
		return time;
	}


	public Entity spawn() {
		
		int count = 0;
		for(Entity entity : location.getWorld().getNearbyEntities(location, 6, 3, 6)){
			if(entity.getType().equals(type)){
				count++;
			}
		}
		Entity entity = null;
		
		if(count < limit){
			entity = location.getWorld().spawnEntity(getLocationWithNoise(), type);
		}
		
		return entity;
	}
	
	public Location getLocationWithNoise(){
		return location.clone().add(Math.random()-1, 0, Math.random()-1);
	}
}
