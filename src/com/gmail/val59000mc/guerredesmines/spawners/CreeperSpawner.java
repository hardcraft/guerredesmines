package com.gmail.val59000mc.guerredesmines.spawners;

import org.bukkit.Location;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class CreeperSpawner extends Spawner {

	public CreeperSpawner(Location location, int limit, int time) {
		super(location, EntityType.CREEPER, limit, time);
	}
	
	@Override
	public Entity spawn() {
		
		Entity entity = super.spawn();
		if(entity != null){
			((Creeper) entity).setHealth(1);
		}
		
		return entity;
	}

}
