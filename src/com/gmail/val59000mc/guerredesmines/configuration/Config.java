package com.gmail.val59000mc.guerredesmines.configuration;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.milkbowl.vault.Vault;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.economy.VaultManager;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.players.BedLocation;
import com.gmail.val59000mc.guerredesmines.spawners.ChickenSpawner;
import com.gmail.val59000mc.guerredesmines.spawners.CreeperSpawner;
import com.gmail.val59000mc.guerredesmines.spawners.SkeletonSpawner;
import com.gmail.val59000mc.guerredesmines.spawners.Spawner;
import com.gmail.val59000mc.guerredesmines.spawners.ZombieSpawner;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class Config {
	
	// Dependencies
	public static boolean isVaultLoaded;
	public static boolean isSIGLoaded;
	public static boolean isBountifulApiLoaded;

	// World 
	public static String lastWorldName;
	public static String worldName;
	public static boolean isLoadLastWord;
	
	// day time
	public static Integer dayTime;

	// Players
	public static int playersToStart;
	public static int maxPlayers;
	public static int countdownToStart;
	public static int killAfterDisconnect;
	
	// Locations
	public static Location lobby;
	public static List<BedLocation> redBeds;
	public static List<BedLocation> blueBeds;
	public static LocationBounds buildBounds;
	public static LocationBounds presenceBounds;
	public static int forbiddenAbove;
	public static LocationBounds beaconBounds;
	public static int beaconMoveUp;
	public static int beaconMoveAfter;
	public static int beaconMoveByBlocks;
	public static Location oreTree;
	public static int oreTreeDelay;
	public static Set<BlockState> oreTreeBlocks;
	public static Set<Location> creeperVillagers;
	public static Block furnace;
	
	// Spawner
	public static Set<Spawner> spawners;
	
	// Bungee
	public static boolean isBungeeEnabled;
	public static String bungeeServer;
	
	// Rewards
	public static double killReward;
	public static double winReward;
	public static double vipRewardBonus;
	public static double vipPlusRewardBonus;
	public static double helperRewardBonus;
	public static double builderRewardBonus;
	public static double moderateurRewardBonus;
	public static double administrateurRewardBonus;
	

	// Items
	public static List<ItemStack> chestItems;
	public static int refillChestsDelay;
	
	public static void load(){
		
		FileConfiguration cfg = GDM.getPlugin().getConfig();

		// Debug
		Logger.setDebug(cfg.getBoolean("debug",false));
		
		Logger.debug("-> Config::load");

		// World
		lastWorldName = cfg.getString("world.last-world","last_wic_world");
		isLoadLastWord = cfg.getBoolean("world.load-last-world",true);
		worldName = "guerredesmines_playing";
		
		// day time
		dayTime = cfg.getInt("day-time",6000);
		
		// Players
		playersToStart = cfg.getInt("players.min",21);
		countdownToStart = cfg.getInt("players.countdown",10);
		maxPlayers = cfg.getInt("players.max",12);
		killAfterDisconnect = cfg.getInt("players.kill-after-disconnect",300);

		// Items
		refillChestsDelay = cfg.getInt("chest.refill-delay",300);

		chestItems = Lists.newArrayList();
		for(String itemStr : cfg.getStringList("chest.items")){
			chestItems.add(Parser.parseItemStack(itemStr));
		}

		
		// Bungee
		isBungeeEnabled = cfg.getBoolean("bungee.enable",false);
		bungeeServer = cfg.getString("bungee.server","lobby");

		// Rewards
		killReward = cfg.getDouble("reward.kill",3);
		winReward = cfg.getDouble("reward.win",25);
		vipRewardBonus = cfg.getDouble("reward.vip",100);
		vipPlusRewardBonus = cfg.getDouble("reward.vip+",100);
		helperRewardBonus = cfg.getDouble("reward.helper",100);
		builderRewardBonus = cfg.getDouble("reward.builder",100);
		moderateurRewardBonus = cfg.getDouble("reward.moderateur",100);
		administrateurRewardBonus = cfg.getDouble("reward.administrateur",100);
		
		// Dependencies
		loadVault();
		loadSIG();
		loadBountifulApi();
		VaultManager.setupEconomy();
		Logger.debug("<- Config::load");
	}
	
	private static void loadVault(){
		Logger.debug("-> Config::loadVault");
		
		Plugin vault = Bukkit.getPluginManager().getPlugin("Vault");
        if(vault == null || !(vault instanceof Vault)) {
            Logger.warn(I18n.get("config.dependency.vault-not-found"));
        	 isVaultLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.vault-loaded"));
        	 isVaultLoaded = true;
        }
		Logger.debug("<- Config::loadVault");
	}
	
	private static void loadBountifulApi(){
		Logger.debug("-> Config::loadBountifulApi");
		
		Plugin bountifulApi = Bukkit.getPluginManager().getPlugin("BountifulAPI");
        if(bountifulApi == null || !(bountifulApi instanceof BountifulAPI)) {
            Logger.warn(I18n.get("config.dependency.bountiful-not-found"));
        	 isBountifulApiLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.bountifulapi-loaded"));
            isBountifulApiLoaded = true;
        }
		Logger.debug("<- Config::loadBountifulApi");
	}

	private static void loadSIG(){
		Logger.debug("-> Config::loadSIG");
		
		Plugin sig = Bukkit.getPluginManager().getPlugin("SimpleInventoryGUI");
        if(sig == null || !(sig instanceof SIG)) {
            Logger.warn(I18n.get("config.dependency.sig-not-found"));
        	 isSIGLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.sig-loaded"));
            isSIGLoaded = true;
        }
		Logger.debug("<- Config::loadSIG");
	}
		
	public static void loadLocations(){
		World world = Bukkit.getWorld(worldName);
		
		FileConfiguration cfg = GDM.getPlugin().getConfig();
		
		// Locations
		lobby = Parser.parseLocation(world, cfg.getString("locations.lobby"));
		
		redBeds = Lists.newArrayList();
		for(String locStr : cfg.getStringList("locations.red")){
			String[] split = locStr.split(":");
			redBeds.add(new BedLocation(Parser.parseLocation(world, split[0]), BlockFace.valueOf(split[1])));
		}
		
		blueBeds = Lists.newArrayList();
		for(String locStr : cfg.getStringList("locations.blue")){
			String[] split = locStr.split(":");
			blueBeds.add(new BedLocation(Parser.parseLocation(world, split[0]), BlockFace.valueOf(split[1])));
		}
		
		buildBounds = new LocationBounds(Parser.parseLocation(world, cfg.getString("locations.build-bounds.min")), Parser.parseLocation(world, cfg.getString("locations.build-bounds.max")));
		presenceBounds = new LocationBounds(Parser.parseLocation(world, cfg.getString("locations.presence-bounds.min")), Parser.parseLocation(world, cfg.getString("locations.presence-bounds.max")));
		beaconBounds = new LocationBounds(Parser.parseLocation(world, cfg.getString("locations.beacon-bounds.min")), Parser.parseLocation(world, cfg.getString("locations.beacon-bounds.max")));

		forbiddenAbove = cfg.getInt("locations.forbidden-above",59);
		beaconMoveUp = cfg.getInt("locations.beacon-move-up",42);
		beaconMoveAfter = cfg.getInt("locations.beacon-move-after",600);
		beaconMoveByBlocks = cfg.getInt("locations.beacon-move-by-blocks",3);
		
		// creeper-villagers
		creeperVillagers = Sets.newHashSet();
		for(String villagerLoc : cfg.getStringList("locations.creeper-villagers")){
			creeperVillagers.add(Parser.parseLocation(world, villagerLoc));
		}
		
		// Spawners
		spawners = Sets.newHashSet();
		
		int chickenLimit = cfg.getInt("spawner.chicken-limit");
		int chickenDelay = cfg.getInt("spawner.chicken-delay");
		for(String chickenLoc : cfg.getStringList("spawner.chicken")){
			spawners.add(new ChickenSpawner(Parser.parseLocation(world, chickenLoc), chickenLimit, chickenDelay));
		}
		
		int creeperLimit = cfg.getInt("spawner.creeper-limit");
		int creeperDelay = cfg.getInt("spawner.creeper-delay");
		for(String creeperLoc : cfg.getStringList("spawner.creeper")){
			spawners.add(new CreeperSpawner(Parser.parseLocation(world, creeperLoc), creeperLimit, creeperDelay));
		}
		
		int skeletonLimit = cfg.getInt("spawner.skeleton-limit");
		int skeletonDelay = cfg.getInt("spawner.skeleton-delay");
		for(String skeletonLoc : cfg.getStringList("spawner.skeleton")){
			spawners.add(new SkeletonSpawner(Parser.parseLocation(world, skeletonLoc), skeletonLimit, skeletonDelay));
		}
		
		int zombieLimit = cfg.getInt("spawner.zombie-limit");
		int zombieDelay = cfg.getInt("spawner.zombie-delay");
		for(String zombieLoc : cfg.getStringList("spawner.zombie")){
			spawners.add(new ZombieSpawner(Parser.parseLocation(world, zombieLoc), zombieLimit, zombieDelay));
		}
		
		// ore tree
		oreTree = Parser.parseLocation(world,cfg.getString("locations.ore-tree"));
		oreTreeDelay = cfg.getInt("locations.ore-tree-delay");
		oreTreeBlocks = new HashSet<BlockState>();
		int startX = oreTree.getBlockX();
		int startY = oreTree.getBlockY();
		int startZ = oreTree.getBlockZ();
		for(int y = startY ; y < startY+6 ; y++){
			for(int x = startX-5 ; x < startX+5 ; x++){
				for(int z = startZ-5 ; z < startZ+5 ; z++){
					Block block = world.getBlockAt(x, y, z);
					switch(block.getType()){
						case DIAMOND_ORE:
						case GOLD_ORE:
						case IRON_ORE:
						case LOG:
						case GRASS:
							oreTreeBlocks.add(block.getState());
							Logger.debug("oreTree, found block "+block.getType()+" at "+Locations.printLocation(block.getLocation()));
							break;
						default:
							break;
					}
				}
			}
		}
		
		furnace = world.getBlockAt(Parser.parseLocation(world, cfg.getString("locations.furnace")));
		
	}
		
}
