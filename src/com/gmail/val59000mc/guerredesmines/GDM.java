package com.gmail.val59000mc.guerredesmines;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.spigotutils.Logger;

public class GDM extends JavaPlugin{
	
	private static GDM pl;
	
	
	public void onEnable(){
		pl = this;
	
		// Blocks players joins while loading the plugin
		Bukkit.getServer().setWhitelist(true);
		saveDefaultConfig();
		
		Logger.setColoredPrefix(ChatColor.WHITE+"["+ChatColor.GREEN+"Guerre des Mines"+ChatColor.WHITE+"]"+ChatColor.RESET+" ");
		Logger.setStrippedPrefix("[Guerre des Mines] ");
		
		Bukkit.getScheduler().runTaskLater(this, new Runnable(){
			
			public void run() {
				GameManager.instance().loadGame();
				
				// Unlock players joins and rely on UhcPlayerJoinListener
				Bukkit.getServer().setWhitelist(false);
			}
			
		}, 1);
		
		
	}
	
	public static GDM getPlugin(){
		return pl;
	}
	
	public void onDisable(){
	}
}
