package com.gmail.val59000mc.guerredesmines.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.chests.ChestManager;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;


public class ChestRefillThread implements Runnable{

	private static ChestRefillThread instance;
	
	private boolean run;
	private ChestManager cm;
	
	public static void start(ChestManager cm){
		Logger.debug("-> ChestRefillThread::start");
		if(instance == null){
			instance = new ChestRefillThread(cm);
			Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), instance, Config.refillChestsDelay*20);
		}
		Logger.debug("<- ChestRefillThread::start");
	}



	public static void stop() {
		instance.run = false;
	}
	
	
	public ChestRefillThread(ChestManager cm){
		this.run = true;
		this.cm = cm;
	}
	
	public void run() {
		
		Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable(){

			public void run(){
				if(run){

					refillChests();
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), ChestRefillThread.this, Config.refillChestsDelay*20);
				}
			}

			private void refillChests() {
				
				Logger.broadcast(I18n.get("game.refill-chests-warning"));
				Sounds.playAll(Sound.PISTON_EXTEND, 2, 2);
				
				Bukkit.getScheduler().runTaskLater(GDM.getPlugin(), new Runnable() {
					
					public void run() {

						Logger.broadcast(I18n.get("game.refill-chests"));
						Sounds.playAll(Sound.PISTON_EXTEND, 2, 2);
						cm.refillChests();
					}
				}, 200);
				
			}
			
		});

	}
}
