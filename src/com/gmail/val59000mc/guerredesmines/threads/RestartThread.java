package com.gmail.val59000mc.guerredesmines.threads;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.spigotutils.Bungee;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Time;

public class RestartThread implements Runnable{

	private static RestartThread instance;
	
	RestartThread thread;
	long remainingTime;
	
	
	public static void start(){
		Logger.debug("-> RestartThread::start");
		if(instance == null){
			Bukkit.getScheduler().runTaskAsynchronously(GDM.getPlugin(), new RestartThread());
		}
		Logger.debug("<- RestartThread::start");
	}
	
	
	public RestartThread(){
		instance = this;
		this.remainingTime = 15;
	}
	
	public void run(){
		
		Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable() {
			
			public void run() {
				if(remainingTime <= 5 || (remainingTime > 0 && remainingTime%5 == 0)){
					Logger.broadcast(I18n.get("game.shutting-down-in").replace("%time%", Time.getFormattedTime(remainingTime)));
				}
				
				remainingTime--;
				
				if(remainingTime == 5){
					if(Config.isBungeeEnabled){
						for(Player player : Bukkit.getOnlinePlayers()){
							Bungee.sendPlayerToServer(GDM.getPlugin(),player,Config.bungeeServer);
						}
					}
				}
				
				if(remainingTime <= 0){
					
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-all");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
				}else{
					Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), instance, 20);
				}
			}
		});		
		
	}

}
