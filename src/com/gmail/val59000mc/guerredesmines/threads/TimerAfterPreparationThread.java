package com.gmail.val59000mc.guerredesmines.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.game.EndCause;
import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.players.GTeam;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.guerredesmines.players.TeamType;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;

public class TimerAfterPreparationThread implements Runnable{

	private static TimerAfterPreparationThread instance;
	
	private int remainingTime;
	private boolean run;
	private GTeam defenseTeam;
	
	
	public static void start(){
		Logger.debug("-> TimerAfterPreparationThread::start");
		Bukkit.getScheduler().runTaskAsynchronously(GDM.getPlugin(), new TimerAfterPreparationThread());
		Logger.debug("<- TimerAfterPreparationThread::start");
	}
	
	public static void stop(){
		Logger.debug("-> TimerAfterPreparationThread::stop");
		if(instance != null){
			instance.run = false;
		}
		Logger.debug("<- TimerAfterPreparationThread::stop");
	}
	
	public static int getRemainingTime(){
		return (instance != null) ? instance.remainingTime : 0;
	}
	
	public TimerAfterPreparationThread(){
		instance = this;
		this.run = true;
		this.remainingTime = Config.beaconMoveAfter;
		
		for(GTeam team : PlayersManager.instance().getTeams()){
			if(team.getType().equals(TeamType.BLUE)){
				this.defenseTeam = team;
			}
		}
	}
	
	public void run() {
		
		if(run){

			if(remainingTime > 0){
				if(remainingTime <= 60 && remainingTime%15 == 0){
					Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable() {
						
						public void run() {
							Logger.broadcast(I18n.get("game.end-in").replace("%time%", Time.getFormattedTime(remainingTime+1)));
							Sounds.playAll(Sound.NOTE_PLING);
						}
					});
				}
				remainingTime--;
				Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), instance,20);
			}else{
				Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable(){

					public void run() {
						GameManager.instance().endGame(EndCause.TEAM_WIN, defenseTeam);
					}
				
				});
			}
			
			UpdateScoreboardThread.setRemainingRushPhase(remainingTime+1);
		}

	}
}