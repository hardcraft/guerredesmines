package com.gmail.val59000mc.guerredesmines.threads;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.Furnace;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.game.Cranes;
import com.gmail.val59000mc.guerredesmines.items.CustomItems;
import com.gmail.val59000mc.spigotutils.Logger;


public class FurnaceFuelThread implements Runnable{

	private static FurnaceFuelThread instance;
	
	private boolean run;
	private Block furnace;
	
	public static void start(){
		Logger.debug("-> FurnaceFuelThread::start");
		if(instance == null){
			instance = new FurnaceFuelThread();
			Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), instance, 50);
		}
		Logger.debug("<- FurnaceFuelThread::start");
	}



	public static void stop() {
		instance.run = false;
	}
	
	
	public FurnaceFuelThread(){
		this.run = true;
		this.furnace = Config.furnace;
	}
	
	public void run() {
		
		Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable(){

			public void run(){
				if(run){

					handleFuelInFurnace();
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), FurnaceFuelThread.this, 50);
				}
			}
		});

	}

	private void handleFuelInFurnace() {

		if(furnace != null){
			Furnace state = (Furnace) furnace.getState();
			FurnaceInventory inv = state.getInventory();
			ItemStack fuel = inv.getFuel();
			if(CustomItems.isFuelItem(fuel)){
				int amount = fuel.getAmount();
				boolean hasClimbed = Cranes.climb();
				
				if(hasClimbed){
					if(amount == 1){
						inv.setFuel(null);
					}else{
						fuel.setAmount(amount-1);
					}
				}
				
			}
		}
		
	}
}
