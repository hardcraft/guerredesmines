package com.gmail.val59000mc.guerredesmines.threads;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.guerredesmines.game.GameState;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Time;

public class KillDisconnectedPlayerThread implements Runnable {
	
	private String playerName;
	private int remainingTime;
	private KillDisconnectedPlayerThread thread;
	
	public static void start(String playerName){
		KillDisconnectedPlayerThread thread = new KillDisconnectedPlayerThread(playerName);
		Logger.broadcast(I18n.get("player.kill-after-disconnect-warning").replace("%player%",playerName).replace("%time%", Time.getFormattedTime(Config.killAfterDisconnect)));
		Bukkit.getScheduler().runTaskAsynchronously(GDM.getPlugin(), thread);
	}
	
	private KillDisconnectedPlayerThread(String playerName) {
		this.playerName = playerName;
		this.remainingTime = Config.killAfterDisconnect;
		this.thread = this;
	}

	public void run() {
		GameManager gm = GameManager.instance();
		
		if(gm.getState().equals(GameState.PLAYING)){
			Player player = Bukkit.getPlayer(playerName);
			if(player == null){
				remainingTime--;
				if(remainingTime <= 0){
					Logger.debug("KillDisconnectedPlayerThread::run - Player "+playerName+" is eliminated.");
					Logger.broadcast(I18n.get("player.kill-after-disconnect").replace("%player%",playerName));
					
					PlayersManager.instance().removePlayer(playerName);
				}else{
					Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), thread, 20);
				}
			}
		}
		
	}

}
