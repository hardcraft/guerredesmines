package com.gmail.val59000mc.guerredesmines.threads;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.spawners.Spawner;
import com.gmail.val59000mc.spigotutils.Logger;


public class SpawnerThread implements Runnable{

	private static Set<SpawnerThread> instances;
	
	private boolean run;
	private Spawner spawner;
	
	public static void start(Set<Spawner> spawners){
		for(Spawner spawner : spawners){
			start(spawner);
		}
	}
	
	public static void start(Spawner spawner){
		Logger.debug("-> SpawnerThread::start");
		if(instances == null){
			instances = Collections.synchronizedSet(new HashSet<SpawnerThread>());
		}
		SpawnerThread thread = new SpawnerThread(spawner);
		instances.add(thread);
		Bukkit.getScheduler().runTaskAsynchronously(GDM.getPlugin(), thread);
		Logger.debug("<- SpawnerThread::start");
	}



	public static void stop() {
		for(SpawnerThread thread : getInstances()){
			thread.run = false;
		}
	}
	
	
	
	public static synchronized Set<SpawnerThread> getInstances() {
		return instances;
	}
	
	public SpawnerThread(Spawner spawner){
		this.spawner = spawner;
		this.run = true;
	}
	
	public void run() {
		
		Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable(){

			public void run(){
				if(run){

					spawner.spawn();
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), SpawnerThread.this, spawner.getTime());
				}
			}
			
		});

	}
}
