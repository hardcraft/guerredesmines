package com.gmail.val59000mc.guerredesmines.threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import com.gmail.val59000mc.spigotutils.Time;

public class UpdateScoreboardThread implements Runnable{
	

	private static UpdateScoreboardThread instance;
	private Map<GPlayer,SimpleScoreboard> scoreboards;
	
	private static int remainingPreparationPhase = 0;
	private static int remainingRushPhase = 0;
	
	
	public static void setRemainingPreparationPhase(int remainingPreparationPhase) {
		UpdateScoreboardThread.remainingPreparationPhase = remainingPreparationPhase;
	}

	public static void setRemainingRushPhase(int remainingRushPhase) {
		UpdateScoreboardThread.remainingRushPhase = remainingRushPhase;
	}

	public static void start(){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		Bukkit.getScheduler().runTaskAsynchronously(GDM.getPlugin(), instance);
	}
	
	public static void add(GPlayer gPlayer){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		instance.addRPlayer(gPlayer);
	}
	
	private void addRPlayer(GPlayer rPlayer){
		if(!scoreboards.containsKey(rPlayer)){
			SimpleScoreboard sc = new SimpleScoreboard("Guerre des Mines");
			if(rPlayer.isOnline()){
				setupTeamColors(sc,rPlayer);
			}
			getScoreboards().put(rPlayer, sc);
		}
	}
	
	private void setupTeamColors(SimpleScoreboard sc, GPlayer rPlayer) {
		PlayersManager pm = PlayersManager.instance();
		
		Scoreboard scoreboard = sc.getBukkitScoreboard();
		
		Objective kills = scoreboard.registerNewObjective("kills", "playerKillCount");
		kills.setDisplaySlot(DisplaySlot.PLAYER_LIST);
		
		Team blue = scoreboard.registerNewTeam("blue");
		Team red = scoreboard.registerNewTeam("red");
		blue.setPrefix(ChatColor.BLUE+"");
		red.setPrefix(ChatColor.RED+"");
		
		// Putting players in colored teams
		for(GPlayer anRPlayer : pm.getPlayers()){
			kills.getScore(anRPlayer.getName()).setScore(anRPlayer.getKills());
				
			if(anRPlayer.isOnline() && anRPlayer.getTeam() != null){
				switch(anRPlayer.getTeam().getType()){
				case BLUE:
					blue.addEntry(anRPlayer.getName());
					break;
				case RED:
					red.addEntry(anRPlayer.getName());
					break;
				default:
					break;
				}
			}
		}
	}
	
	private UpdateScoreboardThread(){
		this.scoreboards = Collections.synchronizedMap(new HashMap<GPlayer,SimpleScoreboard>());
	}
	
	private synchronized Map<GPlayer,SimpleScoreboard> getScoreboards(){
		return scoreboards;
	}
	
	public void run() {
		Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable(){

			public void run() {
					
					for(Entry<GPlayer,SimpleScoreboard> entry : getScoreboards().entrySet()){
						GPlayer rPlayer = entry.getKey();
						SimpleScoreboard scoreboard = entry.getValue();
						
						if(rPlayer.isOnline() && rPlayer.getTeam() != null && rPlayer.isPlaying()){
							updatePlayingScoreboard(rPlayer,scoreboard);
						}else if(rPlayer.isOnline()){
							updateSpectatingScoreboard(rPlayer, scoreboard);
						}
					}

					Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), instance, 20);
				}

			private void updatePlayingScoreboard(GPlayer rPlayer,SimpleScoreboard scoreboard) {
				
				Player player = rPlayer.getPlayer();
				
				List<String> content = new ArrayList<String>();
				content.add(" ");
				
				// Kills / Deaths
				content.add(I18n.get("scoreboard.kills-deaths",player));
				content.add(" "+ChatColor.GREEN+""+rPlayer.getKills()+ChatColor.WHITE+" / "+ChatColor.GREEN+rPlayer.getDeaths());
				
				// Coins
				content.add(I18n.get("scoreboard.coins-earned",player));
				content.add(" "+ChatColor.GREEN+""+rPlayer.getMoney());
				

				// Mine phase
				if(remainingPreparationPhase > 0){
					content.add(I18n.get("scoreboard.mine-phase",player));
					content.add(" "+ChatColor.GREEN+""+Time.getFormattedTime(remainingPreparationPhase));
				}	
				
				// Rush phase
				if(remainingRushPhase > 0){
					content.add(I18n.get("scoreboard.rush-phase",player));
					content.add(" "+ChatColor.GREEN+""+Time.getFormattedTime(remainingRushPhase));
				}							
				
				scoreboard.clear();
				for(String line : content){
					scoreboard.add(line);
				}
				scoreboard.draw();
				scoreboard.send(player);
				
			}
			

			private void updateSpectatingScoreboard(GPlayer rPlayer,SimpleScoreboard scoreboard) {
				
				Player player = rPlayer.getPlayer();
				
				List<String> content = new ArrayList<String>();
				content.add(" ");
				
				// Coins
				content.add(I18n.get("scoreboard.coins-earned",player));
				content.add(" "+ChatColor.GREEN+""+rPlayer.getMoney());
				
				// Mine phase
				if(remainingPreparationPhase > 0){
					content.add(I18n.get("scoreboard.mine-phase",player));
					content.add(" "+ChatColor.GREEN+""+Time.getFormattedTime(remainingPreparationPhase));
				}	
				
				// Rush phase
				if(remainingRushPhase > 0){
					content.add(I18n.get("scoreboard.rush-phase",player));
					content.add(" "+ChatColor.GREEN+""+Time.getFormattedTime(remainingRushPhase));
				}	
				
				
				scoreboard.clear();
				for(String line : content){
					scoreboard.add(line);
				}
				scoreboard.draw();
				scoreboard.send(player);
				
			}
				
			});
				
	}
	
	
	
	
}
