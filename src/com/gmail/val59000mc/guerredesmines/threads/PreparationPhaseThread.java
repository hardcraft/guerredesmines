package com.gmail.val59000mc.guerredesmines.threads;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.GTeam;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.guerredesmines.titles.TitleManager;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;

public class PreparationPhaseThread implements Runnable{

	private static PreparationPhaseThread instance;
	
	private int remainingTime;
	private boolean run;
	
	
	public static void start(){
		Logger.debug("-> PreparationPhaseThread::start");
		if(instance == null){
			Bukkit.getScheduler().runTaskAsynchronously(GDM.getPlugin(), new PreparationPhaseThread());
		}

		FurnaceFuelThread.start();
		
		Logger.debug("<- PreparationPhaseThread::start");
	}
	
	public static void stop(){
		Logger.debug("-> PreparationPhaseThread::stop");
		if(instance != null){
			instance.run = false;
		}
		Logger.debug("<- PreparationPhaseThread::stop");
	}
	
	public PreparationPhaseThread(){
		instance = this;
		this.run = true;
		this.remainingTime = Config.beaconMoveAfter;
	}
	
	public static int getRemainingTime(){
		if(instance == null){
			return 0;
		}
		return instance.remainingTime;
	}
	
	public void run() {
		
		Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable(){

			public void run() {
				
				if(run){
					if(remainingTime > 0 &&  ( remainingTime%60 == 0 || remainingTime < 5 )){
						if(remainingTime < 5){
							Sounds.playAll(Sound.ANVIL_LAND,0.5f, 0.5f);
						}
						Logger.broadcast(I18n.get("game.mine-phase-ends-in").replace("%time%", Time.getFormattedTime(remainingTime)));
					}
					
					remainingTime--;
					
					if(remainingTime < 0){
						endPreparationPhase();
					}else{
						Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), instance, 20);
					}
					
					UpdateScoreboardThread.setRemainingPreparationPhase(remainingTime);
				}
				
			}
		
		});

	}

	private void endPreparationPhase() {
		FurnaceFuelThread.stop();
		TimerAfterPreparationThread.start();
		removeForbiddenBarrier();
		Logger.broadcast(I18n.get("game.mine-phase-end"));
		Sounds.playAll(Sound.ANVIL_USE, 2, 2);
		
		if(Config.isBountifulApiLoaded){

			String title = I18n.get("game.mine-phase-end-title");
			
			for(GTeam team : PlayersManager.instance().getTeams()){
				String subtitle = I18n.get("game.mine-phase-end-subtitle."+team.getType().toString());
				for(GPlayer gPlayer : team.getMembers()){
					if(gPlayer.isOnline()){
						TitleManager.sendTitle(gPlayer.getPlayer(), title, subtitle, 20,  50, 20);
					}
				}
			}
		}
	}

	private void removeForbiddenBarrier() {
		int minX = Config.buildBounds.getMin().getBlockX();
		int maxX = Config.buildBounds.getMax().getBlockX();
		int minZ = Config.buildBounds.getMin().getBlockZ();
		int maxZ = Config.buildBounds.getMax().getBlockZ();
		int y = Config.forbiddenAbove;
		
		World world = Config.buildBounds.getWorld();
		
		for(int x = minX ; x <= maxX ; x++){
			for( int z = minZ ; z <= maxZ ; z++){
				Block block = world.getBlockAt(x, y, z);
				if(block.getType().equals(Material.BARRIER)){
					block.setType(Material.AIR);
				}
			}
		}
	}
	
}