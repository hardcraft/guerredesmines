package com.gmail.val59000mc.guerredesmines.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.guerredesmines.game.GameState;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.TeamType;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;

public class PreventFarAwayPlayerThread implements Runnable {
	
	private static PreventFarAwayPlayerThread thread;
	private boolean run;
	
	public static void start(){
		Logger.debug("-> KillFarAwayPlayerThread::start");
		PreventFarAwayPlayerThread thread = new PreventFarAwayPlayerThread();
		Bukkit.getScheduler().runTaskAsynchronously(GDM.getPlugin(), thread);
		Logger.debug("<- KillFarAwayPlayerThread::start");
	}
	
	private PreventFarAwayPlayerThread() {
		thread = this;
		this.run = true;
	}
	
	public static void stop() {
		thread.run = false;
	}

	public void run() {
		
		Bukkit.getScheduler().runTask(GDM.getPlugin(), new Runnable(){

			public void run() {
				
				if(run){
					
					LocationBounds bounds = Config.presenceBounds;
					int remainingTimeMiningPhase = PreparationPhaseThread.getRemainingTime();
					
					if(bounds != null && bounds.getWorld() != null){

						World world = bounds.getWorld();
						
						for(Player player : Bukkit.getOnlinePlayers()){

							GPlayer rPlayer = PlayersManager.instance().getGPlayer(player);
							
							
							if(rPlayer != null && player.getWorld().equals(world)){

								boolean presenceAllowedAboveMiningLimit = true;
								if(rPlayer.isPlaying() && remainingTimeMiningPhase > 0 && rPlayer.getTeam() != null){
									if(rPlayer.getTeam().getType().equals(TeamType.RED)){
										presenceAllowedAboveMiningLimit = player.getLocation().getBlockY() < Config.forbiddenAbove;
									}else{
										presenceAllowedAboveMiningLimit = player.getLocation().getBlockY() > Config.forbiddenAbove;
									}
								}
								
								if(!presenceAllowedAboveMiningLimit || !bounds.contains(player.getLocation())){
									
									if( !GameManager.instance().isState(GameState.PLAYING) || player.getLocation().getY() >= 0){
										player.teleport(rPlayer.getRespawnLocation());
										Sounds.play(player, Sound.VILLAGER_NO, 2, 2);
										rPlayer.sendI18nMessage("game.leave-arena");
									}else if(GameManager.instance().isState(GameState.PLAYING) 
											&& rPlayer.isPlaying()
											&& player.getLocation().getY() < 0 
											&& player.getHealth() > 0){
										player.setHealth(0);
										Sounds.play(player, Sound.HURT_FLESH, 2, 1);
									}
								}
							}
						}
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(GDM.getPlugin(), thread, 30);
				}
				
				
			}
			
		});
		
	}

}
