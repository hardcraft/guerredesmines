package com.gmail.val59000mc.guerredesmines.listeners;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.items.CustomItems;
import com.gmail.val59000mc.spigotutils.Logger;

public class InteractListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onClickBlock(PlayerInteractEvent event){
		handleC4(event);
	}

	private void handleC4(PlayerInteractEvent event) {
		if(!event.isCancelled()){
			Action action = event.getAction();
			if(action.equals(Action.RIGHT_CLICK_BLOCK)){
				ItemStack item = event.getItem();
				if(CustomItems.isC4(item)){
					
					BlockFace face = event.getBlockFace();
					Block clickedBlock = event.getClickedBlock();
					
					if(clickedBlock != null && face != null){
						
						final Location location = clickedBlock.getRelative(face).getLocation().clone();

						Logger.broadcast(I18n.get("game.explosion").replace("%player%",event.getPlayer().getName()));
						
						Bukkit.getScheduler().runTaskLater(GDM.getPlugin(), new Runnable() {
							
							public void run() {
								CustomItems.explodeC4(location);
							}
						}, 1);
						
					}
					
				}
			}
			
		}
	}
}
