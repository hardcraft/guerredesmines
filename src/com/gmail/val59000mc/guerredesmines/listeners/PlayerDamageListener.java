package com.gmail.val59000mc.guerredesmines.listeners;

import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.guerredesmines.game.GameState;
import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.PlayerState;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;

public class PlayerDamageListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageByEntityEvent event){
		handleFriendlyFire(event);
		handleProjectile(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event){
		handleAnyDamage(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onCreeperDamagedByOtherCreeper(EntityDamageByEntityEvent event){
		
		if(event.getEntity() instanceof Creeper && event.getDamager() instanceof Creeper){
			event.setCancelled(true);
		}
	}
	
	
	///////////////////////
	// EntityDamageEvent //
	///////////////////////

	private void handleAnyDamage(EntityDamageEvent event){
		if(event.getEntity() instanceof Player){
				
			if(!GameManager.instance().isState(GameState.PLAYING)){
				event.setCancelled(true);
			}else{
				if(((Player) event.getEntity()).hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)){
					event.setCancelled(true);
				}
			}
		
		}
	}
	
	///////////////////////////////
	// EntityDamageByEntityEvent //
	///////////////////////////////
	
	private void handleFriendlyFire(EntityDamageByEntityEvent event){

		PlayersManager pm = PlayersManager.instance();
		
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
			
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			
			GPlayer evoDamager = pm.getGPlayer(damager);
			GPlayer evoDamaged = pm.getGPlayer(damaged);
			
			if(evoDamaged != null && evoDamager != null){
				if(evoDamaged.isInTeamWith(evoDamager)){
					event.setCancelled(true);
				}
			}
		}
	}

	private void handleProjectile(EntityDamageByEntityEvent event){

		if(!event.isCancelled()){
			PlayersManager pm = PlayersManager.instance();
			GameManager gm = GameManager.instance();
			
			
			if(event.getEntity() instanceof Player && event.getDamager() instanceof Projectile){
				Projectile arrow = (Projectile) event.getDamager();
				final Player shot = (Player) event.getEntity();
				if(arrow.getShooter() instanceof Player){
					
					if(!gm.isPvp()){
						event.setCancelled(true);
						return;
					}
					
					final Player shooter = (Player) arrow.getShooter();
					GPlayer evoDamager = pm.getGPlayer(shooter);
					GPlayer evoDamaged = pm.getGPlayer(shot);

					if(evoDamager != null && evoDamaged != null){
						if(evoDamager.getState().equals(PlayerState.PLAYING) && evoDamager.isInTeamWith(evoDamaged)){
							event.setCancelled(true);
						}
					}
				}
			}
		}
		
	}
}
