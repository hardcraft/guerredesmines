package com.gmail.val59000mc.guerredesmines.listeners;

import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.GTeam;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;

public class PlayerChatListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerChat(AsyncPlayerChatEvent event){
		
		GPlayer evoPlayer = PlayersManager.instance().getGPlayer(event.getPlayer());
		
		if(evoPlayer != null && evoPlayer.getTeam() != null && !evoPlayer.isGlobalChat()){

			GTeam team = evoPlayer.getTeam();
			
			Set<Player> recipients = event.getRecipients();
			recipients.clear();
			for(GPlayer teammate : team.getMembers()){
				Player teamatePlayer = teammate.getPlayer();
				if(teamatePlayer != null){
					recipients.add(teamatePlayer);
				}
			}
			String teamPrefix = "§f["+team.getType().getColor()+"TEAM"+"§f] §r";
			event.setFormat(teamPrefix+event.getFormat());
		}
		
	}
}
