package com.gmail.val59000mc.guerredesmines.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.economy.VaultManager;
import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.guerredesmines.game.GameState;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;

public class PlayerDeathListener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(final PlayerDeathEvent event) {

		GPlayer wicPlayer = PlayersManager.instance().getGPlayer(event.getEntity());
		
		if(wicPlayer == null || !wicPlayer.isOnline()){
			return;
		}
		
		handleDeathPlayer(event,wicPlayer);
		
	}
	
	public void handleDeathPlayer(final PlayerDeathEvent event, GPlayer dead){
		PlayersManager pm = PlayersManager.instance();
		
		// Add death score to dead player and kill score to killer if playing
		if(GameManager.instance().isState(GameState.PLAYING)){
			
			GPlayer killer = (dead.getPlayer().getKiller() == null) ? null : pm.getGPlayer(dead.getPlayer().getKiller());
			
			dead.addDeath();
			event.setDeathMessage(I18n.get("player.died").replace("%player%",dead.getColor()+dead.getName()));
			
			if(killer != null && killer.isOnline()){
				killer.addKill();
				killer.addMoney(rewardKill(killer.getPlayer()));
				event.setDeathMessage(
						I18n.get("player.killed")
						.replace("%killer%",killer.getColor()+killer.getName())
						.replace("%killed%",dead.getColor()+dead.getName())
				);


				Location location = event.getEntity().getLocation();
				World world = location.getWorld();
				for(ItemStack stack : getLoots(killer.getPlayer())){
					world.dropItem(location, stack);
				}
			}
			
			
			event.setKeepInventory(true);
			event.setKeepLevel(true);
			event.setDroppedExp(0);
			
			Bukkit.getScheduler().runTaskLater(GDM.getPlugin(), new Runnable() {
				
				public void run() {
			        event.getEntity().spigot().respawn();
				}
			}, 20);
		}
		
	}
	
	private Collection<? extends ItemStack> getLoots(Player killer) {
		/*
		hardcraftpvp.guerredesmines.diamond.2
		hardcraftpvp.guerredesmines.diamond.3
		hardcraftpvp.guerredesmines.iron.2
		hardcraftpvp.guerredesmines.iron.3
		hardcraftpvp.guerredesmines.exp.6
		hardcraftpvp.guerredesmines.exp.9
		hardcraftpvp.guerredesmines.wool.2
		hardcraftpvp.guerredesmines.wool.3
		*/
		
		ItemStack diamond = new ItemStack(Material.DIAMOND,1);
		if(killer.hasPermission("hardcraftpvp.guerredesmines.diamond.3")){
			diamond.setAmount(3);
		}else if(killer.hasPermission("hardcraftpvp.guerredesmines.diamond.2")){
			diamond.setAmount(2);
		}
		
		ItemStack iron = new ItemStack(Material.IRON_INGOT,1);
		if(killer.hasPermission("hardcraftpvp.guerredesmines.iron.3")){
			iron.setAmount(3);
		}else if(killer.hasPermission("hardcraftpvp.guerredesmines.iron.2")){
			iron.setAmount(2);
		}
		
		ItemStack exp = new ItemStack(Material.EXP_BOTTLE,3);
		if(killer.hasPermission("hardcraftpvp.guerredesmines.exp.9")){
			exp.setAmount(9);
		}else if(killer.hasPermission("hardcraftpvp.guerredesmines.exp.6")){
			exp.setAmount(6);
		}
		
		ItemStack wool = new ItemStack(Material.WOOL,1);
		if(killer.hasPermission("hardcraftpvp.guerredesmines.wool.3")){
			wool.setAmount(3);
		}else if(killer.hasPermission("hardcraftpvp.guerredesmines.wool.2")){
			wool.setAmount(2);
		}
		
		List<ItemStack> loots = new ArrayList<ItemStack>();
		loots.add(diamond);
		loots.add(iron);
		loots.add(exp);
		loots.add(wool);
		
		return loots;
	}

	private double rewardKill(Player killer) {
		double reward = 0;
		if(Config.isVaultLoaded){
			reward = VaultManager.addMoney(killer, Config.killReward);
			killer.sendMessage(ChatColor.GREEN+"+"+reward+" "+ChatColor.WHITE+"HC");
			Sounds.play(killer, Sound.LEVEL_UP, 0.5f, 2f);
		}
		return reward;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Logger.debug("-> PlayerDeathListener::onPlayerRespawn, player="+event.getPlayer().getName());
		
		GPlayer wicPlayer = PlayersManager.instance().getGPlayer(event.getPlayer());
		
		if(wicPlayer == null){
			return;
		}
		
		handleRespawnPlayer(event,wicPlayer);
		
		Logger.debug("<- PlayerDeathListener::onPlayerRespawn");
	}
	
	public void handleRespawnPlayer(PlayerRespawnEvent event, GPlayer rPlayer){
		Logger.debug("-> PlayerDeathListener::handleRespawnPlayer, player="+rPlayer);

		final String name = rPlayer.getName();
		
		switch(GameManager.instance().getState()){
			case WAITING:
					event.setRespawnLocation(Config.lobby);
					Bukkit.getScheduler().runTaskLater(GDM.getPlugin(), new Runnable() {
					
					public void run() {
						Logger.debug("waitPlayer");
						GPlayer p = PlayersManager.instance().getGPlayer(name);
						if(p != null){
							PlayersManager.instance().waitPlayer(p);
						}
						
					}
				}, 1);
				break;
			case LOADING:
			case STARTING:
			case ENDED:
			default:
			case PLAYING:
				if(rPlayer.getBedLocation() != null){
					event.setRespawnLocation(rPlayer.getRespawnLocation());
				}
				Bukkit.getScheduler().runTaskLater(GDM.getPlugin(), new Runnable() {
					
					public void run() {
						GPlayer p = PlayersManager.instance().getGPlayer(name);
						if(p != null && p.isPlaying()){
							PlayersManager.instance().teamSpawnPlayer(p);
							p.sendI18nMessage("player.respawned");
						}
						
					}
				}, 1);
				break;
		}
		

		Logger.debug("<- PlayerDeathListener::handleRespawnPlayer");
	}
	
}
