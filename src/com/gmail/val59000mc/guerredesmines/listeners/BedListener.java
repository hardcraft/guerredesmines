package com.gmail.val59000mc.guerredesmines.listeners;

import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.material.Bed;

import com.gmail.val59000mc.guerredesmines.players.BedLocation;
import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.spigotutils.Sounds;

public class BedListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEnterBed(final PlayerBedEnterEvent event){
		GPlayer gPlayer = PlayersManager.instance().getGPlayer(event.getPlayer());
		
		if(gPlayer != null && gPlayer.isOnline()){
			Block bed = event.getBed();
			gPlayer.setBedLocation(new BedLocation(bed.getLocation(), ((Bed) bed.getState().getData()).getFacing()));
			gPlayer.sendI18nMessage("player.bed-set");
			Sounds.play(event.getPlayer(), Sound.STEP_WOOL, 2 , 1);
			event.setCancelled(true);
		}
	}
}
