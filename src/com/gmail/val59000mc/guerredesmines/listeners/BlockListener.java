package com.gmail.val59000mc.guerredesmines.listeners;

import java.util.Iterator;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.game.EndCause;
import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.guerredesmines.players.TeamType;
import com.gmail.val59000mc.guerredesmines.threads.PreparationPhaseThread;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;

public class BlockListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockBreak(final BlockBreakEvent event){
		handleBreak(event);
	}
	
	private void handleBreak(BlockBreakEvent event) {

		if(!event.isCancelled()){
			
			GPlayer gPlayer = PlayersManager.instance().getGPlayer(event.getPlayer());
			
			if(gPlayer != null && gPlayer.getTeam() != null){
				
				if(canBreakOrPlace(event.getBlock(), gPlayer)){
					
					if(event.getBlock().getType().equals(Material.BEACON)){
						if(gPlayer.getTeam().getType().equals(TeamType.RED)){
							Logger.broadcast(I18n.get("game.objective-broken-by").replace("%player%", gPlayer.getName()));
							GameManager.instance().endGame(EndCause.TEAM_WIN, gPlayer.getTeam());
						}else{
							event.setCancelled(true);
						}
					}
					
					if(Config.furnace.equals(event.getBlock())){
						event.setCancelled(true);
					}
					
				}else{
					event.setCancelled(true);
					Logger.sendMessage(event.getPlayer(), I18n.get("player.cannot-build-here"));
					Sounds.play(event.getPlayer(), Sound.VILLAGER_NO, 2, 2);
				}
				
			}
			
		}
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockPlace(final BlockPlaceEvent event){
		handlePlace(event);
	}

	private void handlePlace(BlockPlaceEvent event) {
		if(!event.isCancelled()){
			
			GPlayer gPlayer = PlayersManager.instance().getGPlayer(event.getPlayer());
			
			if(gPlayer != null && gPlayer.getTeam() != null){
				
				if(!canBreakOrPlace(event.getBlock(), gPlayer)){
					event.setCancelled(true);
					Logger.sendMessage(event.getPlayer(), I18n.get("player.cannot-build-here"));
					Sounds.play(event.getPlayer(), Sound.VILLAGER_NO, 2, 2);
				}
				
			}
		}
	}

	private boolean canBreakOrPlace(Block block, GPlayer gPlayer){
		
		boolean allowBuidAttHeight = true;
		if(PreparationPhaseThread.getRemainingTime() > 0){
			if(gPlayer.getTeam().getType().equals(TeamType.RED)){
				allowBuidAttHeight = block.getLocation().getBlockY() < Config.forbiddenAbove;
			}else{
				allowBuidAttHeight = block.getLocation().getBlockY() > Config.forbiddenAbove;
			}
		}
		Logger.debug("allowBuidAttHeight="+allowBuidAttHeight);
		
		boolean inBuildBounds = Config.buildBounds.contains(block.getLocation());
			Logger.debug("inBuildBounds="+inBuildBounds);
				
		return allowBuidAttHeight && inBuildBounds;
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockExplode(EntityExplodeEvent event){
		if(!event.isCancelled()){
			handleExplodedBlocks(event.blockList());
			if(event.getEntity().getType().equals(EntityType.CREEPER)){
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockExplode(BlockExplodeEvent event){
		if(!event.isCancelled()){
			handleExplodedBlocks(event.blockList());
		}
	}

	private void handleExplodedBlocks(List<Block> blockList) {
		if(blockList.contains(Config.furnace)){
			blockList.remove(Config.furnace);
		}
		
		Iterator<Block> it = blockList.iterator();
		while(it.hasNext()){
			Block block = it.next();
			if(block.getType().equals(Material.BEACON)){
				it.remove();
			}
		}
	}
}
