package com.gmail.val59000mc.guerredesmines.items;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Lists;

public class CustomItems {
	
	private static final String coalName = ChatColor.RED+"Combustible pour la Grue";
	private static final String c4Name = ChatColor.RED+"C4";
	
	public static ItemStack getCoalItem(){
		ItemStack coal = new ItemStack(Material.COAL, 1, (short) 1);
		ItemMeta meta = coal.getItemMeta();
		meta.setDisplayName(coalName);
		meta.setLore(Lists.newArrayList("§eA insérer dans le moteur de la grue","§epour la faire monter de 5 blocs !"));
		coal.setItemMeta(meta);
		return coal;
	}
	
	public static boolean isFuelItem(ItemStack item){
		if(item == null || item.getType().equals(Material.AIR)){
			return false;
		}
		
		return item.getType().equals(Material.COAL) && coalName.equals(item.getItemMeta().getDisplayName());
	}
	
	public static ItemStack getBlindnessPotion(){
		ItemStack blindness = new ItemStack(Material.POTION, 1, (short) 16422);
		PotionMeta meta = (PotionMeta) blindness.getItemMeta();
		meta.addCustomEffect(new PotionEffect(PotionEffectType.BLINDNESS, 400, 0), true);
		blindness.setItemMeta(meta);
		return blindness;
	}
	
	public static ItemStack getWitherPotion(){
		ItemStack wither = new ItemStack(Material.POTION, 1, (short) 16461);
		PotionMeta meta = (PotionMeta) wither.getItemMeta();
		meta.addCustomEffect(new PotionEffect(PotionEffectType.WITHER, 400, 3), true);
		wither.setItemMeta(meta);
		return wither;
	}
	
	public static ItemStack getC4(){
		ItemStack c4 = new ItemStack(Material.TNT, 1);
		ItemMeta meta = c4.getItemMeta();
		meta.setDisplayName(c4Name);
		meta.setLore(Lists.newArrayList("§eDéclenche instantanément","§eune énorme explosion !"));
		c4.setItemMeta(meta);
		return c4;
	}
	
	public static boolean isC4(ItemStack item){
		if(item == null || item.getType().equals(Material.AIR)){
			return false;
		}
		
		return item.getType().equals(Material.TNT) && c4Name.equals(item.getItemMeta().getDisplayName());
	}

	public static void explodeC4(Location location) {
		if(location != null){
			World world = location.getWorld();
			location.getBlock().setType(Material.AIR);
			world.createExplosion(location, 20F, false);
		}
		
	}
	
}
