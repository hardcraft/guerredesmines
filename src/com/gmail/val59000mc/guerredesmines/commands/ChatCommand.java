package com.gmail.val59000mc.guerredesmines.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.guerredesmines.players.GPlayer;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;

public class ChatCommand implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			GPlayer gPlayer = PlayersManager.instance().getGPlayer((Player) sender);
			
			if(gPlayer != null){
				gPlayer.setGlobalChat(!gPlayer.isGlobalChat());
				gPlayer.sendI18nMessage("command.global-chat."+gPlayer.isGlobalChat());
				return true;
			}
		}
		return false;
	}

}
