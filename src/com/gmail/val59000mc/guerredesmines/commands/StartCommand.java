package com.gmail.val59000mc.guerredesmines.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.guerredesmines.game.GameState;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.threads.WaitForNewPlayersThread;
import com.gmail.val59000mc.spigotutils.Logger;

public class StartCommand implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;
			
			GameState state = GameManager.instance().getState();
			
			switch(state){
				case WAITING:
					if(Bukkit.getOnlinePlayers().size() < 4){
						failStart(player);
					}else{
						boolean forced = WaitForNewPlayersThread.force();
						if(forced){
							Logger.sendMessage(player, I18n.get("command.start.ok"));
						}else{
							failStart(player);
						}
					}
					break;
				default:
					failStart(player);
					break;
			}
		}
		return true;
	}
	
	private void failStart(Player player){
		Logger.sendMessage(player, I18n.get("command.start.not-possible"));
	}

}
