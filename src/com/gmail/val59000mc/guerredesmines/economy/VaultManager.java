package com.gmail.val59000mc.guerredesmines.economy;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.spigotutils.Numbers;

public class VaultManager {
	
    private static Economy economy = null;
    
    public static void setupEconomy(){
    	if(Config.isVaultLoaded){
    		RegisteredServiceProvider<Economy> economyProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
	        if (economyProvider != null) {
	            economy = economyProvider.getProvider();
	        }else{
	        	Bukkit.getLogger().severe("Error trying to load economy provider. Check that you have a economy plugin installed");
	        }
    	}
    }

	
	public static double addMoney(Player player, Double amount){
		final double moneyReceived = getAmountAfterRankBonus(player,amount);
		
		if(Config.isVaultLoaded){
			if(economy == null){
				Bukkit.getLogger().warning("Vault is not loaded ! Couldnt pay "+amount+" to "+player.getName()+" !");
				return 0;
			}else{
				final OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());
				Bukkit.getScheduler().runTaskAsynchronously(GDM.getPlugin(), new Runnable() {
					
					public void run() {
						economy.depositPlayer(offlinePlayer, moneyReceived);
					}
				});
				
				return moneyReceived;
			}
		}
		
		return 0;
	}
	
	private static double getAmountAfterRankBonus(Player player, Double amount){
		
		double rankAmount = amount;
		
		if(player.isOp() || player.hasPermission("hardcraftpvp.reward.administrateur")){
			rankAmount = rankAmount*Config.administrateurRewardBonus/100;
		}else if(player.hasPermission("hardcraftpvp.reward.moderateur")){
			rankAmount = rankAmount*Config.moderateurRewardBonus/100;
		}else if(player.hasPermission("hardcraftpvp.reward.helper")){
			rankAmount = rankAmount*Config.helperRewardBonus/100;
		}else if (player.hasPermission("hardcraftpvp.reward.builder")){
			rankAmount = rankAmount*Config.builderRewardBonus/100;
		}else if(player.hasPermission("hardcraftpvp.reward.vip+")){
			rankAmount = rankAmount*Config.vipPlusRewardBonus/100;
		}else if(player.hasPermission("hardcraftpvp.reward.vip")){
			rankAmount = rankAmount*Config.vipRewardBonus/100;
		}
		
		return Numbers.round(rankAmount, 2);
	}
	

}
