package com.gmail.val59000mc.guerredesmines.i18n;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;

public class English {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();

		// config
		s.put("config.dependency.vault-not-found", "Plugin Vault not found, no support for rewards");
		s.put("config.dependency.vault-loaded", "Plugin Vault loaded.");
		s.put("config.dependency.worldedit-not-found", "Plugin WorldEdit not found, no support for schematics");
		s.put("config.dependency.worldedit-loaded", "Plugin WorldEdit loaded.");
		s.put("config.dependency.sig-not-found", "Plugin SimpleInventoryGUI not found, no support for shops");
		s.put("config.dependency.sig-loaded", "Plugin SimpleInventoryGUI loaded.");
		s.put("config.dependency.bountifulapi-not-found", "Plugin BountifulAPI not found, no support for titles");
		s.put("config.dependency.bountifulapi-loaded", "Plugin BountifulAPI loaded.");
		
		// commands
		s.put("command.global-chat.true", "§aYou are now talking to everyone.");
		s.put("command.global-chat.false", "§aYou are now talking to your team.");
		s.put("command.start.not-possible", "§cYou can't force starting the game.");
		s.put("command.start.ok", "§aGame starting has been forced.");
		
		
	    // parser
		s.put("parser.wrong-location", "Couldn't parser the location %location%");
		
		s.put("parser.item.not-found", "Couldn't find item string");
		s.put("parser.item.empty-string", "Item string is empty");
		s.put("parser.item.wrong-material", "The material name doesn't exist");
		s.put("parser.item.wrong-damage-value", "Wrong damage value");
		s.put("parser.item.wrong-amount", "Wrong amount");
		s.put("parser.item.wrong-enchantment-syntax", "Wrong enchantment syntax");
		s.put("parser.item.wrong-enchantment-name", "Wrong enchantment name");
		s.put("parser.item.wrong-enchantment-level", "Wrong enchantment level");
		
		s.put("parser.playerclass.not-found", "Player class not found");
		s.put("parser.playerclass.items-not-found", "items not found in player class");
		s.put("parser.playerclass.wrong-player-class", "Cannot instantiate player class");
		
					
		// map loader
		s.put("map-loader.load.no-last-world", "No last world to load, creating a new world");
		s.put("map-loader.load.last-world-not-found", "Last world not found, creating a new world.");
		s.put("map-loader.delete.no-last-world", "Not last world to delete.");
		s.put("map-loader.delete.last-world-not-found", "Last world to delete not found.");
		s.put("map-loader.delete", "Deleting last world.");
		s.put("map-loader.copy.not-found", "'winteriscoming' directory not found, cannot copy.");

		// game
		s.put("game.player-allowed-to-join", "Players are now allowed to join");
		s.put("game.start", "The game begins !");
		s.put("game.starting-in", "The game will begin in %time% !");
		s.put("game.end-in", "Defense will win in %time% !");
		s.put("game.end", "The game is finished !");
		s.put("game.end-stopped", "The game continues !");
		s.put("game.shutting-down-in", "Shutting down in %time% !");
		s.put("game.remaining-time", "Time left : %time% !");
		s.put("game.end.team-win", ChatColor.GREEN+"The game is finished ! Winning team : ");
		s.put("game.end.no-more-players", ChatColor.GREEN+"Game has ended because there were no more players left.");
		s.put("game.leave-arena", ChatColor.RED+"Don't try to leave the map.");
		s.put("game.mine-phase-ends-in", ChatColor.GOLD+"Preparation phase ends in %time%");
		s.put("game.warning", ChatColor.RED+"Warning");
		s.put("game.objective-moves", ChatColor.GOLD+"The objective is going up !");
		s.put("game.ore-tree-regen", ChatColor.GREEN+"The ore tree has been regenerated !");
		s.put("game.ore-tree-regen-warning", ChatColor.GOLD+"The ore tree will be regenerated in 10s.");
		s.put("game.explosion", ChatColor.GOLD+"%player% blew up a C4 charge !");
		s.put("game.refill-chests-warning", ChatColor.GOLD+"Refillings chests in 10 seconds !");
		s.put("game.refill-chests", ChatColor.GREEN+"Chests have been refilled !");
		s.put("game.mine-phase-end", ChatColor.GOLD+"Preparation phase is over ! Climb up to destroy the beacon !");
		s.put("game.mine-phase-end-title", ChatColor.GOLD+"Preparation is over !");
		s.put("game.mine-phase-end-subtitle.RED", ChatColor.YELLOW+"Destroy the objective !");
		s.put("game.mine-phase-end-subtitle.BLUE", ChatColor.YELLOW+"Defend the objective !");
		s.put("game.hint-move-bed", ChatColor.AQUA+"Don't forget you can move and sleep in your bed to change your respawn point !");
		s.put("game.objective-broken-by", ChatColor.GOLD+"%player% destroyed the objective !");
		
		// players
		s.put("player.not-allowed-to-join", "You are not allowed to join that game");
		s.put("player.welcome", "Welcome in Mining War game !");
		s.put("player.full", "§cThe game if full. If nobody logs out you will be a spectator !");
		s.put("player.spectate", "You are spectating.");
		s.put("player.RED", ChatColor.RED+"Red team !");
		s.put("player.BLUE", ChatColor.BLUE+"Blue team !");
		s.put("player.coins-earned", ChatColor.GREEN+"HardCoins earned : ");
		s.put("player.ennemy-spawn-forbidden", ChatColor.RED+"You can't attack the ennemies' spawn");
		s.put("player.died", "%player% §rdied");
		s.put("player.killed", "%killer% §rkilled %killed%");
		s.put("player.respawned", "§aYou have respawned at the best bed found.");
		s.put("player.bed-set", "§aYour respawn location has been updated !");
		s.put("player.cannot-build-here", "§cYou are not allowed to build here !");
		s.put("player.reconnect", "§7%player% logged back in the game.");
		s.put("player.kill-after-disconnect-warning", "§7%player% has disconnected. He has up to %time% to reconnect.");
		s.put("player.kill-after-disconnect", "§7%player% didn't log back in the game and has been eliminated.");
		
		// team
		s.put("team.RED", "Attackers");
		s.put("team.BLUE", "Defensors");
				
		// scoreboard
		s.put("scoreboard.kills-deaths", "Kills/Deaths");
		s.put("scoreboard.coins-earned", "Coins");
		s.put("scoreboard.teammates", "Teammates");
		s.put("scoreboard.online-players", "Players");
		s.put("scoreboard.time-to-start", "Starting in");
		s.put("scoreboard.mine-phase", "Preparation");
		s.put("scoreboard.rush-phase", "Fin");

		// ping
		s.put("ping.loading", "Loading");
		s.put("ping.playing", "Playing");
		s.put("ping.starting", "Starting");
		s.put("ping.waiting", "Waiting");
		s.put("ping.ended", "Ended");
				
				
		return s;
	}

}
