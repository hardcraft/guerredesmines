package com.gmail.val59000mc.guerredesmines.i18n;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;

public class French {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();
		
		// config
		s.put("config.dependency.vault-not-found", "Le plugin Vault est introuvable, pas de support pour les recompenses");
		s.put("config.dependency.vault-loaded", "Le plugin Vault a ete correctement charge.");
		s.put("config.dependency.worldedit-not-found", "Le plugin WorldEdit est introuvable, pas de support pour les schematics");
		s.put("config.dependency.worldedit-loaded", "Le plugin WorldEdit a ete correctement charge.");
		s.put("config.dependency.sig-not-found", "Le plugin SimpleInventoryGUI est introuvable, pas de support pour les shops");
		s.put("config.dependency.sig-loaded", "Le plugin SimpleInventoryGUI a ete correctement charge.");
		s.put("config.dependency.bountifulapi-not-found", "Le plugin BountifulAPI est introuvable, pas de support pour les titres");
		s.put("config.dependency.bountifulapi-loaded", "Le plugin BountifulAPI a ete correctement charge.");
		
		// commands
		s.put("command.global-chat.true", "§aTu parles désormais à tout le monde.");
		s.put("command.global-chat.false", "§aTu parles désormais à ta team.");
		s.put("command.start.not-possible", "§cTu ne peux pas forcer le démarrage du jeu.");
		s.put("command.start.ok", "§aLe lancement du jeu a été forcé.");
		
		// parser
		s.put("parser.wrong-location", "Impossible de parser la location %location%");
		
		s.put("parser.item.not-found", "Texte de config l'item non trouve");
		s.put("parser.item.empty-string", "Texte de config l'item vide");
		s.put("parser.item.wrong-material", "La materiau n'existe pas");
		s.put("parser.item.wrong-damage-value", "Erreur de damage value");
		s.put("parser.item.wrong-amount", "Erreur sur la quantite");
		s.put("parser.item.wrong-enchantment-syntax", "Mauvaise syntaxe d'enchantement");
		s.put("parser.item.wrong-enchantment-name", "Mauvais nom d'enchantement");
		s.put("parser.item.wrong-enchantment-level", "Mauvais niveau d'enchantement");
		
		s.put("parser.playerclass.not-found", "Classe de joueur introuvable");
		s.put("parser.playerclass.items-not-found", "items introuvables dans la classe de joueur");
		s.put("parser.playerclass.wrong-player-class", "impossible de creer la classe de joueur");
		
					
		// map loader
		s.put("map-loader.load.no-last-world", "Pas d'ancienne map a charger, creation d'une nouvelle map");
		s.put("map-loader.load.last-world-not-found", "Ancienne map non trouvee, creation d'une nouvelle map");
		s.put("map-loader.delete.no-last-world", "Pas d'ancienne map a supprimer.");
		s.put("map-loader.delete.last-world-not-found", "Ancienne map a supprimer non trouvee.");
		s.put("map-loader.delete", "Suppression de l'ancienne map.");
		s.put("map-loader.copy.not-found", "Dossier 'winteriscoming' introuvable, copie impossible.");
		
		// game
		s.put("game.player-allowed-to-join", "Les joueurs peuvent maintenant rejoindre la partie");
		s.put("game.start", "La partie commence !");
		s.put("game.starting-in", "La partie va commencer dans %time% !");
		s.put("game.end-in", "Les défenseurs vont gagner dans %time% !");
		s.put("game.end", "La partie est terminee !");
		s.put("game.end-stopped", "La partie continue !");
		s.put("game.shutting-down-in", "Arret dans %time% !");
		s.put("game.remaining-time", "Il reste %time% !");
		s.put("game.end.team-win", ChatColor.GREEN+"La partie est finie ! Equipe gagnante : ");
		s.put("game.end.no-more-players", ChatColor.GREEN+"La partie est finie car il n'y a plus de joueurs.");
		s.put("game.leave-arena", ChatColor.RED+"N'essaie pas de sortir de la map.");
		s.put("game.mine-phase-ends-in", ChatColor.GOLD+"La phase de préparation prend fin dans %time%");
		s.put("game.warning", ChatColor.RED+"Attention");
		s.put("game.objective-moves", ChatColor.GOLD+"L'objectif monte !");
		s.put("game.ore-tree-regen", ChatColor.GREEN+"L'arbre à minerai a été regénéré !");
		s.put("game.ore-tree-regen-warning", ChatColor.GOLD+"L'arbre à minerai va être regénéré dans 10s.");
		s.put("game.explosion", ChatColor.GOLD+"%player% a fait exploser une charge de C4 !");
		s.put("game.refill-chests-warning", ChatColor.GOLD+"Remplissage des coffres dans 10 secondes !");
		s.put("game.refill-chests", ChatColor.GREEN+"Les coffres ont été remplis !");
		s.put("game.mine-phase-end", ChatColor.GOLD+"La phase de préparation est terminée ! Montez pour détruire le beacon au bout de la grue !");
		s.put("game.mine-phase-end-title", ChatColor.GOLD+"Préparation terminée !");
		s.put("game.mine-phase-end-subtitle.RED", ChatColor.YELLOW+"Détruis l'objectif !");
		s.put("game.mine-phase-end-subtitle.BLUE", ChatColor.YELLOW+"Défends l'objectif !");
		s.put("game.hint-move-bed", ChatColor.AQUA+"N'oublie pas que tu peux déplacer et dormir dans ton lit pour changer ton point de spawn.");
		s.put("game.objective-broken-by", ChatColor.GOLD+"%player% a détruit l'objectif !");
		
		// players
		s.put("player.not-allowed-to-join", "Vous n'etes pas autorise a rejoindre cette partie.");
		s.put("player.welcome", "Bienvenue dans le jeu Guerre des Mines !");
		s.put("player.full", "§cLa partie est pleine. Si personne ne se deconnecte, tu seras spectateur.");
		s.put("player.spectate", "Vous etes spectateur.");
		s.put("player.RED", ChatColor.RED+"Equipe rouge !");
		s.put("player.BLUE", ChatColor.BLUE+"Equipe bleue !");
		s.put("player.coins-earned", ChatColor.GREEN+"HardCoins gagnés : ");
		s.put("player.ennemy-spawn-forbidden", ChatColor.RED+"Tu ne peux pas attaquer le spawn adverse");
		s.put("player.died", "%player% §rest mort");
		s.put("player.killed", "%killer% §ra tué %killed%");
		s.put("player.respawned", "§aTu as respawn au meilleur lit trouvé.");
		s.put("player.bed-set", "§aTon point de respawn a été mis à jour !");
		s.put("player.cannot-build-here", "§cTu n'as pas le droit de construire ici !");
		s.put("player.reconnect", "§7%player% s'est reconnecté.");
		s.put("player.kill-after-disconnect-warning", "§7%player% s'est déconnecté. Il a jusque %time% pour se reconnecter.");
		s.put("player.kill-after-disconnect", "§7%player% ne s'est pas reconnecté et a été éliminé.");

		// team
		s.put("team.RED", "Attaquants");
		s.put("team.BLUE", "Défenseurs");
		
		// scoreboard
		s.put("scoreboard.kills-deaths", "Tués/Morts");
		s.put("scoreboard.coins-earned", "Coins");
		s.put("scoreboard.teammates", "Coéquipiers");
		s.put("scoreboard.online-players", "Joueurs");
		s.put("scoreboard.time-to-start", "Commence dans");
		s.put("scoreboard.mine-phase", "Prépration");
		s.put("scoreboard.rush-phase", "Fin");
		
		// ping
		s.put("ping.loading", "Chargement");
		s.put("ping.playing", "En jeu");
		s.put("ping.starting", "Commence");
		s.put("ping.waiting", "En attente");
		s.put("ping.ended", "Fin");
		
		return s;
	}

}
