package com.gmail.val59000mc.guerredesmines.players;

import org.bukkit.ChatColor;

public enum TeamType {
	RED(ChatColor.RED),
	BLUE(ChatColor.BLUE);

	private ChatColor color;

	private TeamType(ChatColor color){
		this.color = color;
	}
	
	public ChatColor getColor() {
		return color;
	}
}
