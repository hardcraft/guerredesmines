package com.gmail.val59000mc.guerredesmines.players;

public enum PlayerState {
  WAITING,
  PLAYING,
  DEAD;
}
