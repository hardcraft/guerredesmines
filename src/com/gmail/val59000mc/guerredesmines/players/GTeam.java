package com.gmail.val59000mc.guerredesmines.players;

import java.util.ArrayList;
import java.util.List;

public class GTeam {

	private List<GPlayer> members;
	private TeamType type;
	private int limit;
	private int kills;
	private List<BedLocation> beds;
	
	public GTeam(TeamType type, List<BedLocation> beds) {
		this.members = new ArrayList<GPlayer>();
		this.type = type;
		this.kills = 0;
		this.beds = beds;
	}
	
	public TeamType getType() {
		return type;
	}

	public void setType(TeamType type) {
		this.type = type;
	}

	public void sendI18nMessage(String code) {
		for(GPlayer wicPlayer: members){
			wicPlayer.sendI18nMessage(code);
		}
	}
	
	public boolean contains(GPlayer player){
		return members.contains(player);
	}
	
	public synchronized List<GPlayer> getMembers(){
		return members;
	}

	public List<GPlayer> getPlayingMembers(){
		List<GPlayer> playingMembers = new ArrayList<GPlayer>();
		for(GPlayer uhcPlayer : getMembers()){
			if(uhcPlayer.getState().equals(PlayerState.PLAYING)){
				playingMembers.add(uhcPlayer);
			}
		}
		return playingMembers;
	}
	
	public synchronized List<String> getMembersNames(){
		List<String> names = new ArrayList<String>();
		for(GPlayer player : getMembers()){
			names.add(player.getName());
		}
		return names;
	}
	
	public void addKill() {
		this.kills++;
	}
	
	public int getKills() {
		return this.kills;
	}
	
	public void addPlayer(GPlayer gPlayer){
		gPlayer.leaveTeam();
		gPlayer.setTeam(this);
		getMembers().add(gPlayer);
		if(beds.size() >= getMembers().size()){
			gPlayer.setOriginalBedLocation(beds.get(getMembers().size()-1));
		}
	}

	public boolean isFull(){
		return getMembers().size() == limit;
	}
	
	public void leave(GPlayer gPlayer){
		getMembers().remove(gPlayer);
		gPlayer.setBedLocation(null);
	}
	
	public boolean isOnline(){
		for(GPlayer gPlayer : getMembers()){
			if(gPlayer.isOnline()){
				return true;
			}
		}
		return false;
	}


	public boolean isPlaying() {
		for(GPlayer gPlayer : getMembers()){
			if(gPlayer.isOnline() && gPlayer.isState(PlayerState.PLAYING)){
				return true;
			}
		}
		return false;
	}
	
	public List<GPlayer> getOtherMembers(GPlayer excludedPlayer){
		List<GPlayer> otherMembers = new ArrayList<GPlayer>();
		for(GPlayer uhcPlayer : getMembers()){
			if(!uhcPlayer.equals(excludedPlayer))
				otherMembers.add(uhcPlayer);
		}
		return otherMembers;
	}

	public Boolean is(TeamType type) {
		return getType().equals(type);
	}

}
