package com.gmail.val59000mc.guerredesmines.players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.economy.VaultManager;
import com.gmail.val59000mc.guerredesmines.game.EndCause;
import com.gmail.val59000mc.guerredesmines.game.GameManager;
import com.gmail.val59000mc.guerredesmines.game.GameState;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.guerredesmines.titles.TitleManager;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;

public class PlayersManager {
	private static PlayersManager instance;
	
	private List<GPlayer> players;
	private List<GTeam> teams;
	
	// static
	
	public static PlayersManager instance(){
		if(instance == null){
			instance = new PlayersManager();
		}
		
		return instance;
	}
	
	// constructor 
	
	private PlayersManager(){
		players = Collections.synchronizedList(new ArrayList<GPlayer>());
		teams = Collections.synchronizedList(new ArrayList<GTeam>());
		
		teams.add(new GTeam(TeamType.RED, Config.redBeds));
		teams.add(new GTeam(TeamType.BLUE, Config.blueBeds));
	}
	
	
	// Accessors
	
	public GPlayer getGPlayer(Player player){
		return getGPlayer(player.getName());
	}
	
	public GPlayer getGPlayer(String name){
		for(GPlayer rPlayer : getPlayers()){
			if(rPlayer.getName().equals(name))
				return rPlayer;
		}
		
		return null;
	}
	
	public synchronized List<GPlayer> getPlayers(){
		return players;
	}
	
	public synchronized List<GTeam> getTeams(){
		return teams;
	}


	public List<GTeam> getPlayingTeams() {

		List<GTeam> playingTeams = new ArrayList<GTeam>();

		for(GTeam team : getTeams()){
			if(team.isPlaying()){
				playingTeams.add(team);
			}
		}
		
		return playingTeams;
	}
	
	
	// Methods 
	
	public synchronized GPlayer addPlayer(Player player){
		GPlayer newPlayer = new GPlayer(player);
		getPlayers().add(newPlayer);
		return newPlayer;
	}
	
	public synchronized void removePlayer(Player player){
		removePlayer(player.getName());
	}
	
	public synchronized void removePlayer(String name){
		GPlayer gPlayer = getGPlayer(name);
		if(gPlayer != null){
			gPlayer.leaveTeam();
			getPlayers().remove(gPlayer);
		}
	}
	
	public boolean isPlaying(Player player){
		GPlayer gPlayer = getGPlayer(player);
		if(gPlayer != null){
			return gPlayer.getState().equals(PlayerState.PLAYING);
		}
		return false;
	}

	public boolean isPlayerAllowedToJoin(Player player){
		Logger.debug("-> PlayersManager::isPlayerAllowedToJoin, player="+player.getName());
		GameManager gm = GameManager.instance();
		
		switch(gm.getState()){
				
			case WAITING:
			case PLAYING:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=true, gameState="+gm.getState());
				return true;
			case STARTING:
			case LOADING:
			case ENDED:
			default:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=false, gameState="+gm.getState());
				return false;
		}
	}

	public void playerJoinsTheGame(Player player) {
		Logger.debug("-> PlayersManager::playerJoinsTheGame, player="+player.getName());
		GPlayer gPlayer = getGPlayer(player);
		
		if(gPlayer == null){
			gPlayer = addPlayer(player);
		}
		
		GameState gameState = GameManager.instance().getState();
		switch(gameState){
			case WAITING:
				gPlayer.setState(PlayerState.WAITING);
				break;
			case STARTING:
			case LOADING:
			case ENDED:
				gPlayer.setState(PlayerState.DEAD);
				break;
			case PLAYING:
				if(!gPlayer.isState(PlayerState.PLAYING)){
					gPlayer.setState(PlayerState.DEAD);
				}
				break;
		}
			
		switch(gPlayer.getState()){
			case WAITING:
				Logger.debug("waitPlayer");
				gPlayer.sendI18nMessage("player.welcome");
				if(Config.isBountifulApiLoaded){
					TitleManager.sendTitle(player,ChatColor.GREEN+"Guerre des Mines", 20, 20, 20);
				}
				waitPlayer(gPlayer);
				player.teleport(Config.lobby);
				break;
			case PLAYING:
				Logger.debug("relogPlayer");
				relogPlayer(gPlayer);
				break;
			case DEAD:
			default:
				Logger.debug("spectatePlayer");
				spectatePlayer(gPlayer);
				break;
		}
	}

	public void waitPlayer(GPlayer gPlayer){
		
		if(gPlayer.isOnline()){
			if(Bukkit.getOnlinePlayers().size()>Config.maxPlayers){
				gPlayer.sendI18nMessage("player.full");
			}
			Player player = gPlayer.getPlayer();
			player.setGameMode(GameMode.SPECTATOR);
			player.setAllowFlight(true);
			player.setFlying(true);
			Effects.addPermanent(player, PotionEffectType.SATURATION, 0);
			Effects.addPermanent(player, PotionEffectType.SPEED, 0);
			player.setHealth(20);
		}
		
	}
	
	public void startPlayer(final GPlayer rPlayer){
		rPlayer.setState(PlayerState.PLAYING);
		UpdateScoreboardThread.add(rPlayer);
		rPlayer.teleportToRespawnLocation();
		if(rPlayer.isOnline()){
			Player player = rPlayer.getPlayer();
			player.setFlying(false);
			player.setAllowFlight(false);
			player.setGameMode(GameMode.SURVIVAL);
			Sounds.play(player, Sound.ENDERDRAGON_GROWL, 2, 2);
			Effects.clear(player);
			Inventories.clear(player);
			Inventories.give(player, new ItemStack(Material.STONE_SWORD));
			Inventories.give(player, new ItemStack(Material.STONE_PICKAXE));
			Inventories.give(player, new ItemStack(Material.STONE_AXE));
			Inventories.give(player, new ItemStack(Material.STONE_SPADE));
			Inventories.give(player, new ItemStack(Material.COOKED_BEEF,5));
		}
		teamSpawnPlayer(rPlayer);
	}
	
	/**
	 * Relog a playing player in the game
	 * @param wicPlayer
	 */
	public void relogPlayer(GPlayer gPlayer){
		
		if(gPlayer.isOnline() && GameManager.instance().isState(GameState.PLAYING) && gPlayer.getTeam() != null){
			teamSpawnPlayer(gPlayer);Logger.broadcast(I18n.get("player.reconnect").replace("%player%", gPlayer.getName()));
		}else{
			spectatePlayer(gPlayer);
		}
		
	}

	public void teamSpawnPlayer(GPlayer rPlayer) {
		if(rPlayer.isOnline()){
			Player player = rPlayer.getPlayer();
			player.setGameMode(GameMode.SURVIVAL);
			Effects.add(player, PotionEffectType.DAMAGE_RESISTANCE, 45, 200);
			Effects.add(player, PotionEffectType.INCREASE_DAMAGE, 45, 0);
		}
	}
	
	public void spectatePlayer(GPlayer gPlayer){

		gPlayer.setState(PlayerState.DEAD);
		gPlayer.sendI18nMessage("player.spectate");
		UpdateScoreboardThread.add(gPlayer);
		
		if(gPlayer.isOnline()){
			Player player = gPlayer.getPlayer();
			Inventories.clear(player);
			player.setGameMode(GameMode.SPECTATOR);
			player.teleport(Config.lobby);
		}
	}
	
	public List<GPlayer> getPlayingPlayers() {
		List<GPlayer> playingPlayers = new ArrayList<GPlayer>();
		for(GPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.PLAYING) && p.isOnline()){
				playingPlayers.add(p);
			}
		}
		return playingPlayers;
	}
	
	public List<GPlayer> getWaitingPlayers() {
		List<GPlayer> waitingPlayers = new ArrayList<GPlayer>();
		for(GPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.WAITING) && p.isOnline()){
				waitingPlayers.add(p);
			}
		}
		return waitingPlayers;
	}

	public void startAllPlayers() {
		Logger.debug("-> PlayersManager::startAllPlayers");
		assignRandomTeamsToPlayers();
		refreshVisiblePlayers();
		for(GPlayer gPlayer : getPlayers()){
			if(gPlayer.getTeam() == null){
				spectatePlayer(gPlayer);
			}else{
				startPlayer(gPlayer);
			}
		}
		UpdateScoreboardThread.start();
		Logger.debug("<- PlayersManager::startAllPlayers");
	}
	
	private void refreshVisiblePlayers() {
		for(Player player : Bukkit.getOnlinePlayers()){
			for(Player onePlayer : Bukkit.getOnlinePlayers()){
				player.hidePlayer(onePlayer);
				player.showPlayer(onePlayer);
			}
		}
	}
	
	private void assignRandomTeamsToPlayers(){
		Logger.debug("-> PlayersManager::assignRandomTeamsToPlayers");
		
		synchronized(players){
			// Shuffle players who will play (up to maxPlayers)
			List<GPlayer> playersWhoWillPlay = new ArrayList<GPlayer>(getWaitingPlayers().subList(0, Config.maxPlayers > players.size() ? players.size() : Config.maxPlayers));
			List<GPlayer> playersWhoWillNotPlay = new ArrayList<GPlayer>();
			if(players.size() > Config.maxPlayers){
				playersWhoWillNotPlay = new ArrayList<GPlayer>(getWaitingPlayers().subList(Config.maxPlayers, players.size()));	
			}
			this.players = new ArrayList<GPlayer>();
			Collections.shuffle(playersWhoWillPlay);
			this.players.addAll(playersWhoWillPlay);
			this.players.addAll(playersWhoWillNotPlay);
		}
		
		// j%2
		int nPlayer = 0;
		int j = 0;
		for(GPlayer rPlayer : getWaitingPlayers()){
			nPlayer++;
			if(rPlayer.getTeam() == null && nPlayer <= Config.maxPlayers){
				GTeam team = getTeams().get(j%2);
				team.addPlayer(rPlayer);
	
				if(Config.isBountifulApiLoaded && rPlayer.isOnline())
					TitleManager.sendTitle(rPlayer.getPlayer(), I18n.get("player."+team.getType().toString(),rPlayer.getPlayer()), 10, 20 , 10);
			}
			j++;
		}

		Logger.debug("<- PlayersManager::assignRandomTeamsToPlayers");
	}

	public void endAllPlayers(EndCause cause, GTeam winningTeam) {
		
		if(winningTeam != null){
			for(GPlayer gPlayer : winningTeam.getMembers()){
				if(gPlayer.isOnline()){
					gPlayer.addMoney(VaultManager.addMoney(gPlayer.getPlayer(), Config.winReward));
				}
			}
		}

		for(GPlayer gPlayer : getPlayers()){
			gPlayer.setGlobalChat(true);
			if(gPlayer.isOnline()){
				Player player = gPlayer.getPlayer();
				spectatePlayer(gPlayer);
				Logger.sendMessage(player, I18n.get(cause.getCode(),player)
						+winningTeam.getType().getColor()
						+I18n.get("team."+winningTeam.getType().toString(),player));
				Logger.sendMessage(player, I18n.get("player.coins-earned",player)+gPlayer.getMoney());
			}
		}

		Sounds.playAll(Sound.ENDERDRAGON_GROWL,0.8f,2);
		
	}
	
}
