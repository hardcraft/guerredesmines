package com.gmail.val59000mc.guerredesmines.players;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.spigotutils.Logger;

public class GPlayer {
	private String name;
	private GTeam team;
	private PlayerState state;
	private boolean globalChat;
	private int kills;
	private int deaths;
	private double money;
	private BedLocation originalBedLocation;
	private BedLocation bedLocation;
	
	
	// Constructor
	
	public GPlayer(Player player){
		this.name = player.getName();
		this.team = null;
		this.state = PlayerState.WAITING;
		this.globalChat = false;
		this.kills = 0;
		this.deaths = 0;
		this.money = 0;
		this.originalBedLocation = null;
		this.bedLocation = null;
	}
	
	
	
	public BedLocation getOriginalBedLocation() {
		return originalBedLocation;
	}

	public void setOriginalBedLocation(BedLocation originalBedLocation) {
		this.originalBedLocation = originalBedLocation;
	}

	public ChatColor getColor(){
		if(getTeam() != null){
			return getTeam().getType().getColor();
		}else{
			return ChatColor.WHITE;
		}
	}
	
	// Accessors

	public String getName() {
		return name;
	}
	public BedLocation getBedLocation() {
		return bedLocation;
	}
	public void setBedLocation(BedLocation bedLocation) {
		this.bedLocation = bedLocation;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDeaths() {
		return deaths;
	}
	public void addDeath() {
		this.deaths++;
	}
	
	public synchronized GTeam getTeam() {
		return team;
	}
	public synchronized void setTeam(GTeam team) {
		this.team = team;
	}
	public PlayerState getState() {
		return state;
	}
	public void setState(PlayerState state) {
		this.state = state;
	}
	public double getMoney() {
		return money;
	}
	public void addMoney(double money) {
		this.money += money;
	}
	public boolean isGlobalChat() {
		return globalChat;
	}
	public void setGlobalChat(boolean globalChat) {
		this.globalChat = globalChat;
	}
	public int getKills() {
		return kills;
	}
	public void addKill() {
		this.kills++;
		if(getTeam() != null){
			getTeam().addKill();
		}
	}
	
	// Methods


	public Player getPlayer(){
		return Bukkit.getPlayer(name);
	}
	
	public Boolean isOnline(){
		return Bukkit.getPlayer(name) != null;
	}
	
	public boolean isInTeamWith(GPlayer player){
		return (team != null && team.equals(player.getTeam()));
	}
	
	public void leaveTeam(){
		if(team != null){
			team.leave(this);;
		}
	}

	public void sendI18nMessage(String code) {
		if(isOnline()){
			Logger.sendMessage(getPlayer(), I18n.get(code,getPlayer()));
		}
	}
	
	
	public String toString(){
		return "[name='"+getName()+
				"',team='"+((getTeam() == null) ? null : getTeam().getType())+"'";
	}



	public boolean isTeam(TeamType type) {
		return getTeam() != null && getTeam().getType().equals(type);
	}

	private void updateBedLocationIfBroken(){		
		if(getBedLocation() == null || !getBedLocation().isBedPresent()){
			this.bedLocation = getBestBedLocation();
		}
	}

	private BedLocation getBestBedLocation(){		
		getOriginalBedLocation().createBedAtLocation();
		return getOriginalBedLocation();
	}

	public Location getRespawnLocation() {
		updateBedLocationIfBroken();
		
		if(getBedLocation() == null || getTeam() == null){
			return Config.lobby;
		}
		
		return getBedLocation().getSafeLocation();
	}
	
	
	public void teleportToRespawnLocation() {
		if(isOnline()){
			getPlayer().teleport(getRespawnLocation());
		}
	}



	public boolean isState(PlayerState state) {
		return getState().equals(state);
	}



	public boolean isPlaying() {
		return isOnline() && isState(PlayerState.PLAYING);
	}
	
	public boolean isSpectating() {
		return isOnline() && isState(PlayerState.DEAD);
	}



}
