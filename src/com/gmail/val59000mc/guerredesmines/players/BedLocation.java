package com.gmail.val59000mc.guerredesmines.players;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.material.Bed;

import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Logger;


public class BedLocation {
	private Location bedHeadLocation;
	private BlockFace bedFacing;
	public BedLocation(Location bedHeadLocation, BlockFace bedFacing) {
		super();
		this.bedHeadLocation = bedHeadLocation;
		this.bedFacing = bedFacing;
	}
	
	public Location getBedHeadLocation() {
		return bedHeadLocation;
	}
	
	public boolean isBedPresent(){
		Block bedHeadBlock = bedHeadLocation.getWorld().getBlockAt(bedHeadLocation);
		Block bedFootBlock = bedHeadBlock.getRelative(bedFacing.getOppositeFace());
		
		Block top = bedHeadBlock.getRelative(BlockFace.UP);
		Block topTop = top.getRelative(BlockFace.UP);
		
		return (bedHeadBlock.getType().equals(Material.BED_BLOCK) 
				&& bedFootBlock.getType().equals(Material.BED_BLOCK)
				&& !top.getType().equals(Material.BEACON)
				&& !topTop.getType().equals(Material.BEACON)
			   );
	}
	
	public void createBedAtLocation(){
		Block bedHeadBlock = bedHeadLocation.getBlock();
		Block bedFootBlock = bedHeadBlock.getRelative(bedFacing.getOppositeFace());
		
		Logger.debug("bedHeadBlock="+bedHeadBlock.toString()+" , loc="+Locations.printLocation(bedHeadBlock.getLocation()));
		Logger.debug("bedFootBlock="+bedFootBlock.toString()+" , loc="+Locations.printLocation(bedFootBlock.getLocation()));
		
		BlockState bedFootState = bedFootBlock.getState();
		bedFootState.setType(Material.BED_BLOCK);
		Bed bedFootData = new Bed(Material.BED_BLOCK);
		bedFootData.setHeadOfBed(false);
		bedFootData.setFacingDirection(bedFacing);
		bedFootState.setData(bedFootData);
		bedFootState.update(true);

		BlockState bedHeadState = bedHeadBlock.getState();
		bedHeadState.setType(Material.BED_BLOCK);
		Bed bedHeadData = new Bed(Material.BED_BLOCK);
		bedHeadData.setHeadOfBed(true);
		bedHeadData.setFacingDirection(bedFacing);
		bedHeadState.setData(bedHeadData);
		bedHeadState.update(true);

	}

	public Location getSafeLocation() {
		
		Block head = bedHeadLocation.getBlock();
		Block headUp = head.getRelative(BlockFace.UP);
		headUp.setType(Material.AIR);
		headUp.getRelative(BlockFace.UP).setType(Material.AIR);
		
		return bedHeadLocation.clone().add(0.5, 1, 0.5);
	}
	
	
	
}
