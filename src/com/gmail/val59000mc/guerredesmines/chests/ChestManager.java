package com.gmail.val59000mc.guerredesmines.chests;

import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class ChestManager {
	private Set<BlockState> chests;
	
	public ChestManager(){
		this.chests = Sets.newHashSet();
	}
	
	public void load(){
		Logger.debug("-> ChestManager::load");
		

		
		Location min = Config.buildBounds.getMin();
		Location max = Config.buildBounds.getMax();
		
		int minX = min.getBlockX();
		int minY = Config.forbiddenAbove;
		int minZ = min.getBlockZ();
		
		int maxX = max.getBlockX();
		int maxY = max.getBlockY();
		int maxZ = max.getBlockZ();
		
		World world = Bukkit.getWorld(Config.worldName);

		Logger.debug("Looking for chest in the upper part of the map (above "+Config.forbiddenAbove+")");
		for(int i = minX ; i <= maxX ; i++){
			for(int j = minY ; j <= maxY ; j++){
				for(int k = minZ ; k <= maxZ ; k++){
					Block block = world.getBlockAt(i, j, k);
					if(block.getType().equals(Material.CHEST)){
						Logger.debug("Chest found at "+Locations.printLocation(block.getLocation()));
						chests.add(block.getState());
					}
				}
			}
		}

		Logger.debug("<- ChestManager::load, chests="+chests.size());
	}
	
	public void refillChests(){
		
		Logger.debug("Refilling all chests");

		for(BlockState chest : chests){
			Logger.debug("Refilling chest at "+Locations.printLocation(chest.getLocation()));
			chest.getBlock().setType(Material.CHEST);
			Chest newChest = (Chest) chest.getBlock().getState();
			Inventory inv = newChest.getInventory();
			inv.clear();
			for(ItemStack item : getRandomItems(Config.chestItems, 4)){
				Logger.debug("Adding item "+item.getType());
				inv.addItem(item);
			}
			newChest.update(true);
		}
		
		
	}

	private List<ItemStack> getRandomItems(List<ItemStack> itemsRef, int amount) {
		List<ItemStack> items = Lists.newArrayList();
		int maxSize = itemsRef.size();
		if(maxSize > 0){
			for(int i=0 ; i<amount ; i++){
				ItemStack toAdd = itemsRef.get(Randoms.randomInteger(0, maxSize-1)).clone();
				if(toAdd.getType().equals(Material.BOW)){
					items.add(new ItemStack(Material.ARROW,5));
				}
				items.add(toAdd);
			}
		}

		items.add(new ItemStack(Material.STONE,4));
		
		return items;
	}
}
