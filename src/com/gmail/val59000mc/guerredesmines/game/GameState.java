package com.gmail.val59000mc.guerredesmines.game;

public enum GameState {
	LOADING,
	WAITING,
	STARTING,
	PLAYING,
	ENDED;
}
