package com.gmail.val59000mc.guerredesmines.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.Listener;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.chests.ChestManager;
import com.gmail.val59000mc.guerredesmines.commands.ChatCommand;
import com.gmail.val59000mc.guerredesmines.commands.StartCommand;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.listeners.BedListener;
import com.gmail.val59000mc.guerredesmines.listeners.BlockListener;
import com.gmail.val59000mc.guerredesmines.listeners.InteractListener;
import com.gmail.val59000mc.guerredesmines.listeners.PingListener;
import com.gmail.val59000mc.guerredesmines.listeners.PlayerChatListener;
import com.gmail.val59000mc.guerredesmines.listeners.PlayerConnectionListener;
import com.gmail.val59000mc.guerredesmines.listeners.PlayerDamageListener;
import com.gmail.val59000mc.guerredesmines.listeners.PlayerDeathListener;
import com.gmail.val59000mc.guerredesmines.maploader.MapLoader;
import com.gmail.val59000mc.guerredesmines.players.GTeam;
import com.gmail.val59000mc.guerredesmines.players.PlayersManager;
import com.gmail.val59000mc.guerredesmines.spawners.CreeperVillager;
import com.gmail.val59000mc.guerredesmines.threads.CheckRemainingPlayerThread;
import com.gmail.val59000mc.guerredesmines.threads.ChestRefillThread;
import com.gmail.val59000mc.guerredesmines.threads.PreparationPhaseThread;
import com.gmail.val59000mc.guerredesmines.threads.PreventFarAwayPlayerThread;
import com.gmail.val59000mc.guerredesmines.threads.RestartThread;
import com.gmail.val59000mc.guerredesmines.threads.SpawnerThread;
import com.gmail.val59000mc.guerredesmines.threads.TimerAfterPreparationThread;
import com.gmail.val59000mc.guerredesmines.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.guerredesmines.threads.WaitForNewPlayersThread;
import com.gmail.val59000mc.spigotutils.Logger;


public class GameManager {

	private static GameManager instance = null;
	
	private GameState state;
	private boolean pvp;
	private ChestManager cm;
	
	// static
	
	public static GameManager instance(){
		if(instance == null){
			instance = new GameManager();
		}
		return instance;
	}

	
	// constructor 
	
	private GameManager(){
		this.state = GameState.LOADING;
		this.pvp = false;
	}

	
	// accessors
	
	public GameState getState() {
		return state;
	}
	
	

	public boolean isPvp() {
		return pvp;
	}


	public boolean isState(GameState state) {
		return getState().equals(state);
	}
	
	// methods 
	
	public void loadGame() {
		state = GameState.LOADING;
		Logger.debug("-> GameManager::loadNewGame");
		Config.load();
		
		MapLoader.deleteOldPlayersFiles();
		MapLoader.load();
		
		createCreeperVillagers();
		
		cm = new ChestManager();
		cm.load();
		cm.refillChests();
		
		registerCommands();
		registerListeners();
		
		if(Config.isBungeeEnabled)
			GDM.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(GDM.getPlugin(), "BungeeCord");

		waitForNewPlayers();
		Logger.debug("<- GameManager::loadNewGame");
	}


	
	private void createCreeperVillagers() {
		for(Location location : Config.creeperVillagers){
			new CreeperVillager(location).spawn();
		}
		
	}


	private void registerListeners(){
		Logger.debug("-> GameManager::registerListeners");
		// Registers Listeners
		List<Listener> listeners = new ArrayList<Listener>();		
		listeners.add(new PlayerConnectionListener());	
		listeners.add(new PlayerChatListener());
		listeners.add(new PlayerDamageListener());
		listeners.add(new PlayerDeathListener());
		listeners.add(new BedListener());
		listeners.add(new PingListener());
		listeners.add(new InteractListener());
		listeners.add(new BlockListener());
		for(Listener listener : listeners){
			Logger.debug("Registering listener="+listener.getClass().getSimpleName());
			Bukkit.getServer().getPluginManager().registerEvents(listener, GDM.getPlugin());
		}
		Logger.debug("<- GameManager::registerListeners");
	}
	
	private void registerCommands(){
		Logger.debug("-> GameManager::registerCommands");
		// Registers Listeners	
		GDM.getPlugin().getCommand("chat").setExecutor(new ChatCommand());
		GDM.getPlugin().getCommand("start").setExecutor(new StartCommand());
		Logger.debug("<- GameManager::registerCommands");
	}




	private void waitForNewPlayers(){
		Logger.debug("-> GameManager::waitForNewPlayers");
		WaitForNewPlayersThread.start();
		PreventFarAwayPlayerThread.start();
		UpdateScoreboardThread.start();
		state = GameState.WAITING;
		Logger.infoC(I18n.get("game.player-allowed-to-join"));
		Logger.debug("<- GameManager::waitForNewPlayers");
	}
	
	public void startGame(){
		Logger.debug("-> GameManager::startGame");
		state = GameState.STARTING;
		Logger.broadcast(I18n.get("game.start"));
		PlayersManager.instance().startAllPlayers();	
		playGame();
		Logger.debug("<- GameManager::startGame");
	}
	
	private void playGame(){
		state = GameState.PLAYING;
		pvp = true;

		ChestRefillThread.start(cm);
		CheckRemainingPlayerThread.start();
		PreparationPhaseThread.start();
		SpawnerThread.start(Config.spawners);
		hintMoveBed();
	}
	
	private void hintMoveBed() {
		Bukkit.getScheduler().runTaskLater(GDM.getPlugin(), new Runnable(	) {
			
			public void run() {
				Logger.broadcast(I18n.get("game.hint-move-bed"));
			}
		}, 1800);
	}


	public void endGame(EndCause cause, GTeam winningTeam) {
		if(state.equals(GameState.PLAYING)){
			state = GameState.ENDED;
			pvp = false;
			TimerAfterPreparationThread.stop();
			ChestRefillThread.stop();
			CheckRemainingPlayerThread.stop();
			PreparationPhaseThread.stop();
			PreventFarAwayPlayerThread.stop();
			PlayersManager.instance().endAllPlayers(cause,winningTeam);
			RestartThread.start();
			SpawnerThread.stop();
		}
		
	}

	
	


	
	
	

}
