package com.gmail.val59000mc.guerredesmines.game;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.guerredesmines.GDM;
import com.gmail.val59000mc.guerredesmines.configuration.Config;
import com.gmail.val59000mc.guerredesmines.i18n.I18n;
import com.gmail.val59000mc.guerredesmines.titles.TitleManager;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;

public class Cranes{

	private static Cranes instance;
	
	private int remainingSteps;
	private LocationBounds bounds;
	
	
	public static boolean climb(){
		if(instance == null){
			instance = new Cranes();
		}
		
		if(instance.remainingSteps > 0){
			instance.moveUp();
			return true;
		}

		
		return false;
	}
	
	public Cranes(){
		this.remainingSteps = (Config.beaconMoveUp / Config.beaconMoveByBlocks);
		this.bounds = Config.beaconBounds;
	}
	
	private void moveUp(){

		if(Config.isBountifulApiLoaded){
			TitleManager.sendAllActionBar(I18n.get("game.objective-moves"));
		}else{
			Logger.broadcast(I18n.get("game.warning"));
			Logger.broadcast(I18n.get("game.objective-moves"));
		}
		
		// Running move thread in another thread to return moveUp quickly
		Bukkit.getScheduler().runTaskLater(GDM.getPlugin(), new Runnable() {
			
			public void run() {
				
				remainingSteps--;

				int minX = bounds.getMin().getBlockX();
				int minY = bounds.getMin().getBlockY();
				int minZ = bounds.getMin().getBlockZ();
				
				int maxX = bounds.getMax().getBlockX();
				int maxY = bounds.getMax().getBlockY();
				int maxZ = bounds.getMax().getBlockZ();
				
				int moveBy = Config.beaconMoveByBlocks;
				
				World world = bounds.getWorld();
				
				for(int y = maxY ; y >= minY ; y--){
					for(int x = maxX ; x >= minX ; x--){
						for(int z = maxZ ; z >= minZ ; z--){
							Block toCopy = world.getBlockAt(x, y, z);
							if(!toCopy.getType().equals(Material.AIR)){
								world.getBlockAt(x, y+moveBy, z).setType(toCopy.getType());
								toCopy.setType(Material.AIR);
							}
						}
					}
				}
				
				Sounds.playAll(Sound.ANVIL_USE,0.5f, 0.5f);
				Sounds.playAll(Sound.ANVIL_BREAK,0.5f, 0.5f);
				Sounds.playAll(Sound.ZOMBIE_WOODBREAK,0.5f, 0.5f);
				Sounds.playAll(Sound.CREEPER_HISS,2,0.5f);
				Sounds.playAll(Sound.FUSE,2,0.5f);
				
				bounds.shift(new Vector(0,moveBy,0));
				
				// another sound 5 ticks later
				Bukkit.getScheduler().runTaskLater(GDM.getPlugin(), new Runnable() {
					
					public void run() {

						Sounds.playAll(Sound.HURT_FLESH,0.5f, 0.5f);
						Sounds.playAll(Sound.BLAZE_DEATH,0.5f, 0.5f);
						Sounds.playAll(Sound.ANVIL_LAND,0.5f, 0.5f);
						Sounds.playAll(Sound.CREEPER_HISS,2,0.5f);
						Sounds.playAll(Sound.FUSE,2,0.5f);
						
					}
				}, 5);
				
			}
		}, 1);
		
		
	}
	
	
	
}